<?php namespace App\Interfaces;

interface Comentario
{

    /**
     * Faz um novo comentario.
     *
     * @param array $dados dados do comentário.
     *
     * @return static
     */
    public static function novoComentario(array $dados);

    /**
     * Edita o comentário de uma publicação.
     * @param  array  $dados        dados do comentário.
     * @param  INT $idComentario [description]
     * @return [type]               [description]
     */
    public static function editarComentario(array $dados, $idComentario);
}