<?php namespace App\Interfaces;

interface Interesse
{
    /**
    * Cadastra um novo interesse para o usuário.
    *
    * @param array $interesses com os novos interesses do usuário.
    * @param int $id_perfil_usuario perfil do usuário.
    *
    * @return mixed
    */
    public static function cadastrarInteresse(array $interesses, $id_perfil_usuario);


    /**
     * Retorna os interesses de um perfil.
     *
     * @param INT $id_perfil_usuario perfil do usuário.
     *
     * @return mixed
     */
    public static function getInteresses($id_perfil_usuario);

    /**
     * Atualiza os interesses de um perfil.
     *
     * @param array $interesses interesses
     * @param int $id_perfil_usuario perfil do usuário.
     *
     * @return mixed
     */
    public static function atualizarInteresses(array $interesses, $id_perfil_usuario);

    /**
     * Exclui os interesses de um usuário.
     *
     * @param INT $id_perfil_usuario perfil do usuário.
     *
     * @return mixed
     */
    public static function excluirInteresses($id_perfil_usuario);
}