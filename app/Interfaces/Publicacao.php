<?php namespace App\Interfaces;

interface Publicacao
{

    /**
     * Faz uma nova publicação.
     *
     * @param array $dados dados da publicação.
     *
     * @return mixed
     */
    public static function novaPublicacao(array $dados);

    /**
     * Retorna uma quantidade de publicações.
     *
     * @param int|null $qnt
     * @param Bollean true|false valida pra ver se está buscando por ajax.
     * @param int $id_usuario usuário atual.
     * @return mixed
     */
    public static function getPublicacoes($id_usuario, $buscandoPorAjax = false, $qnt = null);

    /**
     * Busca novas atualizações.
     * 
     * @param  array $publicacoes atualizações por padrão.
     * @return array              Com as novas atualizações..
     */
    public static function buscarNovasAtualizacoes($publicacoes);

    /**
     * Comenta uma publicacao.
     *
     * @param array $dados dados do comentario.
     *
     * @return mixed
     */
    public static function comentarPublicacao(array $dados);

}