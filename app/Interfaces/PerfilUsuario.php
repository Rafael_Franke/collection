<?php namespace App\Interfaces;

interface PerfilUsuario
{

    /**
     * Cria um novo perfil ao cadastrar usuário.
     * @param  array  $dados      array com os dados do usuário.
     * @param  int $id_usuario usuário a ser criado o perfil.
     * @return array             dados do perfil.
     */
    public static function criarPerfil(array $dados);

    /**
     * Atualiza perfil de um usuário.
     * @param  array  $dadosPerfil dados do perfil do usuário.
     * @param  int    $id_usuario usuario a ter o perfil atualizado.
     * @return array    dados atualizados do perfil do usuário.
     */
    public static function atualizarPerfil(array $dadosPerfil, $id_usuario);

    /**
     * Retorna o perfil de um usuário.
     * @param  int $id_usuario perfil deste usuário.
     * @return array             dados do perfil.
     */
    public static function getPerfil($id_usuario);
}