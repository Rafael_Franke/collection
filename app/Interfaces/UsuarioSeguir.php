<?php namespace App\Interfaces;

interface UsuarioSeguir
{
    /**
     * Segue um novo usuário.
     * @param  array $dados dados dos envolventes usuários.
     * @return boolean                 true|false
     */
    public static function seguirUsuario(array $dados);

    /**
     * Pega todos os usuários que o usuário atual segue.
     * @param  int $id_usuario usuário setado na sessão.
     * @return array             com os usuários.
     */
    public static function getUsuariosSeguindo($id_usuario);
}