<?php namespace App\Interfaces;

interface Colecao
{
    /**
     * Cadastra uma nova coleção.
     *
     * @param array $dados dados da coleção.
     * @param int $id_usuario usuário a qual essa coleção pertence.
     *
     * @return mixed
     */
    public static function cadastrarColecao(array $dados, $id_usuario);

}