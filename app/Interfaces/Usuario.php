<?php namespace App\Interfaces;

interface Usuario
{
    /**
     * Retorna todos os usuários.
     * @return [Collection] [todos os usuários]
     */
    public static function getUsuarios();


    /**
     * Cadastra um novo usuário
     * @param  array  $dados dados do usuário
     * @return boolean        true|false
     */
    public static function cadastrarUsuario(array $dados);


    /**
     * Retorna um usuário pelo o id.
     * @param  int $id id do usuário.
     * @return objeto     com dados do usuário.
     */
    public static function getUsuario($id);


    /**
     * Retorna um usuário pelo o id.
     * @param  int $id id do usuário.
     * @return objeto     com dados do usuário.
     */
    public static function getUsuarioById($id);

    /**
     * Atualiza os dados do usuário.
     *
     * @param array $dadosUsuario
     * @param $id_usuario
     *
     * @return mixed
     */
    public static function atualizarDados(array $dadosUsuario, $id_usuario);

    /**
     * Loga usuário.
     * @param  array $dadosUsuario dados do usuário
     * @return boolean      true|false
     */
    public static function logar(array $dadosUsuario);


    /**
     * Verifica a existencia de um login no banco de dados.
     *
     * @param STRING $login
     * @param bool $retornaDados se essa váriavel for verdade ela nos retornaremos os resultado encontrado.
     *
     * @return mixed
     */
    public static function existeLogin($login, $retornaDados = false);

    /**
     * Lista todos os usuários que o usuario da sessão não segue.
     *
     * @param int $idUsuario
     *
     * @return mixed
     */
    public static function listarUsuariosNaoSeguidos($idUsuario);
}