<?php namespace App\Actions\Usuario\Colecao;

use App\Models\Usuario\Colecao\ColecaoModel;

use App\Interfaces\Colecao;

/**
 * Class ColecaoAction
 * @package App\Actions\Usuario\Colecao
 */
class ColecaoAction extends ColecaoModel implements Colecao
{

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Cadastra uma nova coleção.
     *
     * @param array $dados dados da coleção.
     * @param int $id_usuario usuário a qual essa coleção pertence.
     *
     * @return mixed
     */
    public static function cadastrarColecao(array $dados, $id_usuario)
    {
        $dados['id_usuario'] = $id_usuario;

        return ColecaoModel::create($dados);
    }
}