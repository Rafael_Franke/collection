<?php namespace App\Actions\Usuario;

use App\Models\Usuario\UsuarioSeguirModel;
use App\Actions\Usuario\UsuarioAction;
use App\Interfaces\UsuarioSeguir;

/**
 * Classe responsável por executar as ações relacionadas ao usuário.
 *
 * Class UsuarioAction
 * @package App\Actions\Usuario
 */
class UsuarioSeguirAction extends UsuarioSeguirModel implements UsuarioSeguir
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Segue um novo usuário.
     * @param  array $dados dados dos envolventes usuários.
     * @return boolean                 true|false
     */
    public static function seguirUsuario(array $dados)
    {
        $usuarioSeguidor = UsuarioAction::getUsuarioByLogin($dados['usuario_seguindo']);
        $dados['usuario_seguindo'] = $usuarioSeguidor->id_usuario;

        return UsuarioSeguirModel::create($dados);
    }


    /**
     * Pega todos os usuários que o usuário atual segue.
     * @param  int $id_usuario usuário setado na sessão.
     * @return array             com os usuários.
     */
    public static function getUsuariosSeguindo($id_usuario)
    {
        return UsuarioSeguirModel::where('id_usuario', $id_usuario)->get();
    }


    public static function usuarioSeguires($idUsuario)
    {
        return UsuarioSeguirModel::where('usuario_seguindo', $idUsuario)->get();
    }



}