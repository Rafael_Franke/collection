<?php namespace App\Actions\Usuario;

use App\Models\Usuario\ComentarioModel;
use App\Interfaces\Comentario;

/**
 * Class PublicacaoAction
 * @package App\Actions\Usuario
 */
class ComentarioAction extends ComentarioModel implements Comentario
{

    public function __construct()
    {

    }

    /**
     * Faz um novo comentario.
     *
     * @param array $dados dados do comentário.
     *
     * @return static
     */
    public static function novoComentario(array $dados)
    {
        return ComentarioModel::create($dados);
    }



    /**
     * Edita o comentário de uma publicação.
     * @param  array  $dados        dados do comentário.
     * @param  INT $idComentario [description]
     * @return [type]               [description]
     */
    public static function editarComentario(array $dados, $idComentario)
    {
        return false;
    }

}