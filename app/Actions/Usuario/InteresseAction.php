<?php namespace App\Actions\Usuario;

use App\Models\Usuario\InteresseModel;
use App\Interfaces\Interesse;


/**
 * Classe responsável por executar ações relacionadas ao interesse do usuário.
 *
 * Class InteresseAction
 * @package App\Actions\Usuario
 */
class InteresseAction extends InteresseModel implements Interesse
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Cadastra um novo interesse para o usuário.
    *
    * @param array $interesses com os novos interesses do usuário.
    * @param int $id_perfil_usuario perfil do usuário.
    *
    * @return mixed
    */
    public static function cadastrarInteresse(array $interesses, $id_perfil_usuario)
    {
        foreach($interesses as $interesse)
        {
            $interesses = explode(',', $interesse);
        }

        self::excluirInteresses($id_perfil_usuario);
        foreach($interesses as $interesse)
        {
            if(!empty($interesse))
            {
                $dadosInteresse['interesse'] = $interesse;
                $dadosInteresse['id_perfil_usuario'] = $id_perfil_usuario;
                InteresseModel::create($dadosInteresse);
            }
        }

        return true;
    }

    /**
     * Retorna os interesses de um perfil.
     *
     * @param INT $id_perfil_usuario perfil do usuário.
     *
     * @return mixed
     */
    public static function getInteresses($id_perfil_usuario)
    {
        return InteresseModel::where('id_perfil_usuario', '=', $id_perfil_usuario)->get();
    }

    /**
    * Atualiza os interesses de um perfil.
    *
    * @param array $interesses interesses
    * @param int $id_perfil_usuario perfil do usuário.
    *
    * @return mixed
    */
    public static function atualizarInteresses(array $interesses, $id_perfil_usuario)
    {
        return true;
    }

    /**
    * Exclui os interesses de um usuário.
    *
    * @param INT $id_perfil_usuario perfil do usuário.
    *
    * @return mixed
    */
    public static function excluirInteresses($id_perfil_usuario)
    {
        return InteresseModel::where('id_perfil_usuario', '=', $id_perfil_usuario)->delete();
    }
}