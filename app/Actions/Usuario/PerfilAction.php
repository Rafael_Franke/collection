<?php namespace App\Actions\Usuario;

use App\Models\Usuario\PerfilUsuarioModel;
use App\Interfaces\PerfilUsuario;

/**
 * Classe responsável pelas ações relacionadas ao perfil do usuário.
 */
class PerfilAction implements PerfilUsuario
{
    /**
     * Cria um novo perfil ao cadastrar usuário.
     * @param  array  $dados      array com os dados do usuário.
     * @param  int $id_usuario usuário a ser criado o perfil.
     * @return array             dados do perfil.
     */
    public static function criarPerfil(array $dados)
    {
        $criou = PerfilUsuarioModel::create($dados);

        $dadosUsuario = PerfilUsuarioModel::where('id_usuario', $criou->id)->first();

        return $dadosUsuario;
    }

    /**
     * Atualiza o perfil de um usuário.
     *
     * @param array $dados dados para perfil.
     * @param int $id_usuario
     *
     * @return mixed
     */
    public static function atualizarPerfil(array $dados, $id_usuario)
    {
        return PerfilUsuarioModel::where('id_usuario', '=', $id_usuario)->update($dados);
    }

    /**
     * Retorna o perfil de um usuário.
     * @param  int $id_usuario perfil deste usuário.
     * @return array             dados do perfil.
     */
    public static function getPerfil($id_usuario)
    {
        return PerfilUsuarioModel::where('id_usuario', $id_usuario)->first();
    }
}