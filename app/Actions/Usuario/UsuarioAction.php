<?php namespace App\Actions\Usuario;

use App\Models\Usuario\UsuarioModel;
use App\Models\Usuario\PerfilUsuarioModel;
use App\Services\Usuario\Senha;
use App\Services\Usuario\UsuarioLogin;
use App\Actions\Usuario\UsuarioSeguirAction;
use App\Interfaces\Usuario;
use App\Services\DataHora;

/**
 * Classe responsável por executar as ações relacionadas ao usuário.
 *
 * Class UsuarioAction
 * @package App\Actions\Usuario
 */
class UsuarioAction extends UsuarioModel implements Usuario
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Função retorna todos os usuários
     * @return [type] [description]
     */
    public static function getUsuarios()
    {
        return UsuarioModel::all();
    }

    /**
     * Cadastra um novo usuário.
     * @param  array  $dados dados do usuário
     * @return boolean        true|false
     */
    public static function cadastrarUsuario(array $dados)
    {
        $usuario['email'] = $dados['email'];
        $usuario['login'] = $dados['email'];
        $usuario['nome'] = $dados['nome'];
        $usuario['sobrenome'] = $dados['sobrenome'];
        $usuario['password'] = Senha::gerarSenha($dados['password']);

        DataHora::dataTimeStamp(['data_cadastro', 'data_atualizacao'], $usuario);

        $criou = UsuarioModel::create($usuario);

        if($criou)
        {
            $dadosLogin['login'] = $usuario['email'];
            $dadosLogin['password'] = $usuario['password'];
            $dadosLogin['id_usuario'] = $criou->id_usuario;
            return self::logar($dadosLogin);
        }
        else
        {
            return false;
        }
    }

    /**
     * Retorna dados do usuário. Se for login retorna true ou false.
     * @param  array  $dadosUsuario dados do usuário.
     * @param  boolean $login        true|false
     * @return object|boolean         dados do usuário ou dados do true|false
     */
    public static function getUsuario($dadosUsuario, $login = true)
    {
        $usuario = UsuarioModel::where('email', $dadosUsuario['login'])->take(1)->first();
        if(!$login)
            return $usuario;

        $senhaHash = Senha::checarSenha($dadosUsuario['senha'], $usuario->password);
        if(!is_string($senhaHash))
            return $dadosUsuario;

        $perfil = PerfilUsuarioModel::where('id_usuario', $usuario->id_usuario)->first();
        $dadosSessao['login'] = $usuario->login;
        $dadosSessao['nome'] = $usuario->nome;
        $dadosSessao['id_usuario'] = $usuario->id_usuario;
        $dadosSessao['id_perfil_usuario'] = $perfil->id_perfil_usuario;
        $dadosSessao['descricao_perfil'] = $perfil->sobre_voce;

        return self::logar($dadosSessao);
    }

    /**
     * Retorna um usuário pelo o id.
     * @param  int $id id do usuário.
     * @return objeto     com dados do usuário.
     */
    public static function getUsuarioById($id)
    {
        $usuario = UsuarioModel::where('id_usuario', $id)->with(['perfil'])->first();

        return $usuario;
    }

    /**
     * Retorna usuário pelo login
     * @return [type] [description]
     */
    public static function getUsuarioByLogin($login)
    {
        $usuario = UsuarioModel::where('login', $login)->first();

        return $usuario;
    }


    /**
     * Atualiza os dados do usuário.
     *
     * @param array $dadosUsuario
     * @param $id_usuario
     *
     * @return mixed
     */
    public static function atualizarDados(array $dadosUsuario, $id_usuario)
    {
        return UsuarioModel::where('id_usuario', '=', $id_usuario)->update($dadosUsuario);
    }

    /**
     * Loga usuário.
     * @param  array $dadosUsuario dados do usuário
     * @return boolean      true|false
     */
    public static function logar(array $dadosUsuario)
    {
        return UsuarioLogin::logar($dadosUsuario);
    }

    /**
     * Verifica a existencia de um login no banco de dados.
     *
     * @param STRING $login
     * @param bool $retornaDados se essa váriavel for verdade ela nos retornaremos os resultado encontrado.
     *
     * @return mixed
     */
    public static function existeLogin($login, $retornaDados = false)
    {
        $retorno = UsuarioModel::where('login', '=', $login)->first();
        if($retornaDados)
            return $retorno;

        if(!$retorno)
            return false;

        return true;
    }

    /**
     * Lista todos os usuários que o usuario da sessão não segue.
     *
     * @param int $idUsuario
     *
     * @return mixed
     */
    public static function listarUsuariosNaoSeguidos($idUsuario)
    {
        $quemEsteUsuarioSegue = UsuarioSeguirAction::getUsuariosSeguindo($idUsuario)->lists('usuario_seguindo');
        $quemEsteUsuarioSegue[] = $idUsuario;

        return UsuarioModel::whereNotIn('id_usuario', $quemEsteUsuarioSegue)->get();
    }


    public static function seguidoresUsuario($idUsuario)
    {
        $seguidores = UsuarioSeguirAction::usuarioSeguires($idUsuario);
        $idSeguidores = $seguidores->lists('id_usuario');

        $seguidores = UsuarioModel::whereIn('id_usuario', $idSeguidores)->get();

        return $seguidores;
    }


}