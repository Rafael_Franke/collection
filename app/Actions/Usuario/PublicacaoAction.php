<?php namespace App\Actions\Usuario;

use App\Models\Usuario\PublicacaoModel;
use App\Interfaces\Publicacao;
use DB;

/**
 * Class PublicacaoAction
 * @package App\Actions\Usuario
 */
class PublicacaoAction extends PublicacaoModel implements Publicacao
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Faz uma nova publicação.
     *
     * @param array $dados dados da publicação.
     *
     * @return mixed
     */
    public static function novaPublicacao(array $dados)
    {
        $publicou = PublicacaoModel::create($dados);
        $dadosRetorno = [];
        if($publicou)
        {
            $publicacao = PublicacaoModel::where('id_publicacao', '=', $publicou->id_publicacao)->first();
            $dadosRetorno['publicacao'] = $publicacao;
            $dadosRetorno['usuario'] = UsuarioAction::getUsuarioById($publicacao->id_usuario);

            return $dadosRetorno;
        }

        return ['erro' => true];
    }

    /**
     * Retorna as publicações junto com os comentários.
     *
     * @param int|null $qnt
     * @param Bollean true|false valida pra ver se está buscando por ajax.
     * @param int $id_usuario usuário atual.
     * @return mixed
     */
    public static function getPublicacoes($id_usuario, $buscandoPorAjax = false, $qnt = null)
    {
        $qnt = !is_null($qnt) ? $qnt : 20;

        $usuariosSeguidosPeloUsuarioAtual = UsuarioSeguirAction::getUsuariosSeguindo($id_usuario);
        $id_usuarios_seguidos = $usuariosSeguidosPeloUsuarioAtual->lists('usuario_seguindo');
        $id_usuarios_seguidos[] = $id_usuario;

        $publicacoes = PublicacaoModel::with(['usuario', 'comentarios' => function($query){

            $query->with(['usuario'])->take(10)->orderBy('data_hora', 'DESC');

        }])->whereIn('id_usuario', $id_usuarios_seguidos)
            ->take($qnt)
            ->orderBy('data_publicacao', 'DESC');


        if(!$buscandoPorAjax)
        {
            $publicacoes = $publicacoes->get();
            $idsPublicacoes = $publicacoes->lists('id_publicacao');

            return $publicacoes;
        }

        return self::buscarNovasAtualizacoes($publicacoes);
    }

    /**
     * Busca novas atualizações.
     * @param  array $publicacoes query padrão das atualizações dos usuários que o usuário corrente segue.
     * @return array              Com as novas atualizações..
     */
    public static function buscarNovasAtualizacoes($publicacoes)
    {
        $publicacoes = $publicacoes->where('nova', 1)->get();

        return self::tratarDadosParaExibicao($publicacoes);
    }

    /**
     * VALIDAR
     * @param $idsPublicacoes
     */
    public static function atualizaPublicacaoParaAntiga($idsPublicacoes)
    {
        PublicacaoModel::whereIn('id_publicacao', $idsPublicacoes)->update(['nova' => 0]);
    }

    /**
     * validar
     */
    public static function tratarDadosParaExibicao($publicacoes)
    {
        $arrayPublicacoes  = [];
        foreach($publicacoes as $key => $publicacao)
        {
            $arrayPublicacoes[] = array(
                'usuario' => $publicacao->usuario->login,
                'texto' => $publicacao->texto,
                'data_publicacao' => date('d-m-Y', strtotime($publicacao->data_publicacao))
            ) ;
        }

        return $arrayPublicacoes;
    }

    /**
     * Comenta uma publicacao.
     *
     * @param array $dados dados do comentario.
     *
     * @return mixed
     */
    public static function comentarPublicacao(array $dados)
    {
        return ComentarioAction::novoComentario($dados);
    }



}