<?php namespace App\Http\Controllers\Explorar;

use App\Http\Controllers\Controller;
use App\Services\Sessao\Sessao;
use App\Actions\Usuario\UsuarioAction;

class ExplorarController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        Sessao::checar();
    }

    /**
     * Retorna coleções do usuário
     *
     * @return view
     */
    public function index()
    {
        return view('explorar.index');
    }


    /**
     * Explorar colecionadores
     *
     * @return view
     */
    public function colecionadores()
    {
        $colecionadores = UsuarioAction::listarUsuariosNaoSeguidos(Sessao::get('id_usuario'));

        return view('explorar.colecionadores', compact('colecionadores'));
    }

}
