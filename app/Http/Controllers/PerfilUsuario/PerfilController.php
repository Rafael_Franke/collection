<?php namespace App\Http\Controllers\PerfilUsuario;

use Input;
use Response;
use App\Http\Controllers\Controller;
use App\Services\Requests\RequestValidate;
use App\Services\Sessao\Sessao;
use App\Actions\Usuario\PerfilAction;
use App\Actions\Usuario\UsuarioAction;
use App\Actions\Usuario\InteresseAction;
use App\Actions\Usuario\PublicacaoAction;

class PerfilController extends Controller
{

    /**
     * Retorna perfil de um usuário.
     * @param  [string] $usuario usuário requisitado.
     * @return array          dados do perfil.
     */
    public function index($usuario)
    {
        $usuario = UsuarioAction::getUsuarioByLogin($usuario);
        
        $publicacoes = PublicacaoAction::getPublicacoes($usuario->id_usuario, false, 3);
        
        return view('usuario.perfil', compact('publicacoes', 'usuario'));
    }

    /**
     * Atualizar perfil do usuário.
     * @return JSON com informações atualizadas do usuário.
     */
    public function atualizarPerfil()
    {
        RequestValidate::is_ajax();

        $login = snake_case(Input::get('login'));
        $login = str_replace(' ', '-', $login);

        $dadosUsuario = array(
            'email' => Input::get('email'),
            'nome' => Input::get('nome_usuario'),
            'login' => $login
        );

        UsuarioAction::atualizarDados($dadosUsuario, Sessao::get('id_usuario'));

        $dadosInteresse = array(
            'interesse' => Input::get('interesse')
        );
        
        InteresseAction::cadastrarInteresse($dadosInteresse, Sessao::get('id_perfil_usuario'));

        $dadosPerfil = array(
            'sobre_voce' => Input::get('sobre_voce', ''),
            'interesse' => Input::get('interesse', ''),
            'profissao' => Input::get('profissao', ''),
            'sou_fa' => Input::get('sou_fa', ''),
            'id_usuario' => Sessao::get('id_usuario')
        );

        $atualizou = PerfilAction::atualizarPerfil($dadosPerfil, Sessao::get('id_usuario'));
        if(!$atualizou)
        {
            $dados['mensagem'] = 'Erro ao atualizar perfil. Tente novamente mais tarde.';
            $dados['classe'] = 'erroMsg';
            return response()->json(['dados' => $dados]);
        }

        $dados['mensagem'] = 'Perfil atualizado com sucesso!';
        $dados['classe'] = 'sucesso';

        return response()->json(['dados' => $dados]);
    }

}
