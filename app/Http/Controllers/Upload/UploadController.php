<?php namespace App\Http\Controllers\Upload;

use Input;
use Response;
use App\Http\Controllers\Controller;
use App\Services\Requests\RequestValidate;
use App\Services\Sessao\Sessao;
use App\Actions\Usuario\PerfilAction;
use App\Actions\Usuario\UsuarioAction;
use App\Actions\Usuario\InteresseAction;

class UploadController extends Controller
{

    public function index()
    {
        return view('usuario.upload');
    }
}
