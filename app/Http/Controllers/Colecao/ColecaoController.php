<?php namespace App\Http\Controllers\Colecao;

use App\Services\Sessao\Sessao;
use Input;
use Response;
use App\Http\Controllers\Controller;
use App\Services\Requests\RequestValidate;
use App\Actions\Usuario\Colecao\ColecaoAction;

class ColecaoController extends Controller
{

    /**
     * Retorna coleções do usuário
     *
     * @return view
     */
    public function index()
    {
        $colecoes = ColecaoAction::all();

        return view('timeline.colecao.colecoes', compact('colecoes'));
    }

    /**
     * Retorna view para cadastrar uma nova coleção.
     *
     * @return \Illuminate\View\View
     */
    public function adicionarColecao()
    {
        return view('colecao.cadastrar');
    }

    /**
     * Cadastra uma nova coleção para o usuário.
     *
     * @return mixed
     * @throws \App\Services\Requests\Exception
     */
    public function postAdicionarColecao()
    {
        RequestValidate::is_ajax();

        $dados = Input::all();

        $adicionou = ColecaoAction::cadastrarColecao($dados, Sessao::get('id_usuario'));
        if($adicionou)
            return response()->json(['url' => route('adicionar_item')]);

        $dados['mensagem'] = 'Erro ao executar ação.';
        $dados['classe'] = 'erroMsg';

        return response()->json(['dados' => $dados]);
    }


    public function adicionarItem()
    {
        return view('colecao.itens.cadastrar');
    }

}
