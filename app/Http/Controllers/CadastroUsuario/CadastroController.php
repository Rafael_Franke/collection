<?php namespace App\Http\Controllers\CadastroUsuario;

use Input;
use Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use App\Services\Requests\RequestValidate;
use App\Services\Sessao\Sessao;
use App\Actions\Usuario\UsuarioAction;
use App\Actions\Usuario\PerfilAction;

class CadastroController extends Controller
{

    /**
     * Retorna view para cadastro de usuários
     *
     * @return view
     */
    public function index()
    {
        return view('usuario.cadastro');
    }

    /**
     * Cadastra um novo usuário.
     * @return boolean true|false
     */
    public function cadastrar()
    {
        $cadastrar = UsuarioAction::cadastrarUsuario(Input::all());

        $dadosPerfil['id_usuario'] = Sessao::get('id_usuario');
        $dadosPerfil['sobre_voce'] = '';
        $dadosPerfil['interesse'] = '';
        $dadosPerfil['profissao'] = '';
        $dadosPerfil['sou_fa'] = '';

        PerfilAction::criarPerfil($dadosPerfil);
        if($cadastrar)
        {
            return redirect('home');
        }
    }


    /**
     * Verifica se um email existe.
     * @return JSON|boolean json true|false da existência do email
     */
    public function validarEmail()
    {
        RequestValidate::is_ajax();

        $existe = false;
        $email = Input::get('email');
        $resultado = UsuarioAction::where('email', $email)->first();
        if($resultado)
        {
            $existe = true;
            return response()->json(['existe' => $existe]);
        }

        return response()->json(['existe' => $existe]);
    }

}
