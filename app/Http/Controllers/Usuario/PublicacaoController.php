<?php namespace App\Http\Controllers\Usuario;

use App\Services\Sessao\Sessao;
use Input;
use Response;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\Requests\RequestValidate;
use App\Actions\Usuario\PublicacaoAction;


class PublicacaoController extends Controller
{

	/**
	 * Retorna view para fazer login
	 *
	 * @return view
	 */
	public function publicar()
	{
        RequestValidate::is_ajax();

        $dados = Input::all();
        $dados['id_usuario'] = Sessao::get('id_usuario');
        $dados['texto'] = $dados['publicacao'];

        $publicou = PublicacaoAction::novaPublicacao($dados);
        $publicou['publicacao']->data_publicacao = 'Agora mesmo';

        if(!$publicou)
        {
            $dados['mensagem'] = 'Erro ao publicar. Tente novamente mais tarde.';
            $dados['classe'] = 'erroMsg';
            return response()->json(['dados' => $dados]);
        }

        return response()->json(['dados' => $publicou]);
    }


    /**
     * Busca novas atualizações do usuário.
     *
     * @throws \App\Services\Requests\Exception
     */
    public function buscarAtulizacoes()
    {
        RequestValidate::is_ajax();
        $publicacoes = PublicacaoAction::getPublicacoes(Sessao::get('id_usuario'), true, 30);

        return response()->json(['dados' => $publicacoes]);
    }


    public function comentar()
    {
        RequestValidate::is_ajax();

        $dados['id_publicacao'] = Input::get('post');
        $dados['id_usuario'] = Sessao::get('id_usuario');
        $dados['comentario'] = Input::get('comentario');

        PublicacaoAction::comentarPublicacao($dados);
    }


}
