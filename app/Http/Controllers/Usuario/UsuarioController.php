<?php namespace App\Http\Controllers\Usuario;

use App\Actions\Usuario\UsuarioAction;
use Input;
use Response;
use App\Http\Controllers\Controller;
use App\Services\Requests\RequestValidate;
use App\Services\Sessao\Sessao;
use App\Actions\Usuario\UsuarioSeguirAction;
use App\Actions\Usuario\InteresseAction;

class UsuarioController extends Controller
{

    public function seguirUsuario()
    {
    	RequestValidate::is_ajax();
        $dados['usuario_seguindo'] = Input::get('usuario');
        $dados['id_usuario'] = Sessao::get('id_usuario');
        $seguiu = UsuarioSeguirAction::seguirUsuario($dados);

        if(!$seguiu)
        	return response()->json(['seguiu' => false]);

        return response()->json(['seguiu' => true]);
    }


    public function seguidores($usuario)
    {
        $usuario = UsuarioAction::getUsuarioByLogin($usuario);
        $seguidores = UsuarioAction::seguidoresUsuario($usuario->id_usuario);

        return view('usuario.seguidores.index', compact('seguidores', 'usuario'));
    }

}
