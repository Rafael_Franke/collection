<?php namespace App\Http\Controllers\Usuario;

use Input;
use Response;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\Requests\RequestValidate;
use App\Actions\Usuario\UsuarioAction;

class LoginController extends Controller
{

    public $usuarioAct;

    public function _construct()
    {
        parent::__construct();
    }

	/**
	 * Retorna view para fazer login
	 *
	 * @return view
	 */
	public function index()
	{
		return view('login.index');
	}

    /**
     * Loga um usuário
     * @return redirect to home or to login page
     */
    public function logar()
    {
        $dados = Input::all();
        $usuarioLogar = UsuarioAction::getUsuario($dados);
        if(!is_array($usuarioLogar))
            return redirect('timeline');

        return redirect()->route('login')->with('dados', $usuarioLogar);
    }


    /**
     * Valida a existencia de um login no banco de dados.
     *
     * @return mixed
     * @throws \App\Services\Requests\Exception
     */
    public function validarLogin()
    {
        RequestValidate::is_ajax();

        $existeLogin = UsuarioAction::existeLogin(Input::get('valor'));

        return response()->json(['existeLogin' => $existeLogin]);
    }


}
