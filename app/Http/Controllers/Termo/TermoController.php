<?php namespace App\Http\Controllers\Termo;

use App\Http\Controllers\Controller;

class TermoController extends Controller
{

    /**
     * Retorna view com o termo padrão de uso do serviço
     *
     * @return view
     */
    public function index()
    {
        return view('termos.index');
    }

}
