<?php namespace App\Http\Controllers\Home;

use Input;
use Response;
use App\Services\Requests\RequestValidate;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use App\Services\Sessao\Sessao;
use App\Actions\Usuario\UsuarioAction;
use App\Actions\Usuario\PublicacaoAction;

class HomeController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		Sessao::checar();
	}

	/**
	 * Retorna TIMELINE do serviço
	 *
	 * @return view
	 */
	public function index()
	{
		$usuario = UsuarioAction::getUsuarioById(Sessao::get('id_usuario'));
        $publicacoes = PublicacaoAction::getPublicacoes(Sessao::get('id_usuario'), false, 30);
		return view('timeline.home', compact('usuario', 'publicacoes'));
	}

}
