<?php

/**
 * Página de login
 */
Route::get('/', 'Usuario\LoginController@index');
Route::get('timeline/{usuario}', ['as' => 'perfil.index', 'uses' => 'PerfilUsuario\PerfilController@index']);


/**
 * Termos
 */
Route::group(['prefix' => 'termos', 'namespace' => 'Termo'], function () {
    Route::get('/', ['as' => 'termo.index', 'uses' => 'TermoController@index']);
});


/**
 * Cadastro de usuários
 */
Route::group(['prefix' => 'cadastrar', 'namespace' => 'CadastroUsuario'], function () {
    Route::get('/', ['as' => 'cadastro.index', 'uses' => 'CadastroController@index']);
    Route::post('cadastrar', ['as' => 'cadastro.adicionar.post', 'uses' => 'CadastroController@cadastrar']);
    Route::post('valida-email', ['as' => 'valida_email.post', 'uses' => 'CadastroController@validarEmail']);
});


/**
 * Timeline
 */
Route::group(['prefix' => 'timeline', 'namespace' => 'Home'], function() {
    Route::get('/', ['as' => 'timeline.index', 'uses' => 'HomeController@index']);
});


/**
 * Explorar
 */
Route::group(['prefix' => 'explorar', 'namespace' => 'Explorar'], function() {
    Route::get('/', ['as' => 'explorar.index', 'uses' => 'ExplorarController@index']);
    Route::get('colecionadores', ['as' => 'explorar.colecionadores', 'uses' => 'ExplorarController@colecionadores']);
});




/**
 * Coleções
 */
Route::group(['prefix' => 'colecao', 'namespace' => 'Colecao'], function() {
    Route::get('/', ['as' => 'colecao.index', 'uses' => 'ColecaoController@index']);
    Route::get('/nova-colecao', ['as' => 'adicionar_colecao', 'uses' => 'ColecaoController@adicionarColecao']);
    Route::post('/adicionar-colecao', ['as' => 'adicionar_colecao.post', 'uses' => 'ColecaoController@postAdicionarColecao']);
    Route::get('/adicionar-item/{item}', ['as' => 'adicionar_item', 'uses' => 'ColecaoController@adicionarItem']);
});

/**
 * Rotas relacionadas ao perfil do usuário.
 */
Route::group(['prefix' => 'perfil', 'namespace' => 'PerfilUsuario'], function() {
    Route::get('{usuario}', ['as' => 'perfil_ver.index', 'uses' => 'PerfilController@index']);
    Route::post('alterar-perfil', ['as' => 'perfil.alterar.post', 'uses' => 'PerfilController@atualizarPerfil']);
});


/**
 * Controle de sessão e ações do usuário.
 */
Route::group(['prefix' => 'usuario', 'namespace' => 'Usuario'], function() {
    Route::get('/', ['as' => 'login.index', 'uses' => 'LoginController@index']);
    Route::post('logar', ['as' => 'login.post', 'uses' => 'LoginController@logar']);
    Route::post('validar-login', ['as' => 'validar_login.post', 'uses' => 'LoginController@validarLogin']);
    Route::post('publicar', ['as' => 'publicar.post', 'uses' => 'PublicacaoController@publicar']);
    Route::post('comentar', ['as' => 'comentar.post', 'uses' => 'PublicacaoController@comentar']);
    Route::post('seguir', ['as' => 'seguir.post', 'uses' => 'UsuarioController@seguirUsuario']);
    Route::post('publicacoes', ['as' => 'publicacoes.post', 'uses' => 'PublicacaoController@buscarAtulizacoes']);
    Route::get('seguidores/{usuario}', ['as' => 'seguidores.index', 'uses' => 'UsuarioController@seguidores']);
});

Route::group(['prefix' => 'upload', 'namespace' => 'Upload'], function() {
    Route::get('/', ['as' => 'upload.index', 'uses' => 'UploadController@index']);
});







Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
