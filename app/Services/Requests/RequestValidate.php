<?php namespace App\Services\Requests;

use Illuminate\Support\Facades\Request;

class RequestValidate
{

    /**
     * Verifica se o request é ajax. Se não for retorna o não permitido.
     * @return boolean true|false;
     */
    public static function is_ajax()
    {

        if(!Request::ajax())
        {
            throw new Exception("Não permitido!", 1);
            exit;
        }
    }
}