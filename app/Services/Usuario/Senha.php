<?php namespace App\Services\Usuario;

use Illuminate\Support\Facades\Hash;

class Senha
{

    /**
     * Gera uma nova senha para o usuário.
     * @param  [String] $senha senha do usuário
     * @return Hash      Hash da nova senha
     */
    public static function gerarSenha($senha)
    {
        return Hash::make($senha);
    }


    public static function checarSenha($senha, $hash)
    {
        if(!Hash::check($senha, $hash))
            return false;

        return $hash;
    }
}