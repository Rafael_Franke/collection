<?php namespace App\Services\Usuario;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Events\Dispatcher;
use App\Services\Sessao\Sessao;

class UsuarioLogin
{

    public static function logar($dadosUsuario, $manterLogado = false)
    {
        $logado = Sessao::criar($dadosUsuario, 2);

        return $logado;
    }
}