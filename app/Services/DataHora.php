<?php namespace App\Services;

class DataHora
{

    /**
     * Adiciona á uma variável os campos data do tipo timestamp.
     * @param  array  $campos           campos do banco do tipo timestamp
     * @param  array &$variavelInserir array no qual será inserido os campos
     * @return array                   array atualizado.
     */
    public static function dataTimeStamp(array $campos, &$variavelInserir)
    {
        foreach($campos as $campo)
        {
            $variavelInserir[$campo] = date('Y-m-d H:s:i');
        }

        return $variavelInserir;
    }
}