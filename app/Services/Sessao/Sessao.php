<?php namespace App\Services\Sessao;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
class Sessao extends Session
{

    /**
     * Checa dados da sessão.
     * @return redirect ou true|false
     */
    public static function checar()
    {
        $dadosSessao = Session::all();
        if(!isset($dadosSessao['login']) || !isset($dadosSessao['password']))
        {
            redirect()->route('login.index', true);
        }
    }

    /**
     * Cria uma nova sessão para usuário
     * @param  array  $dados dados da sessão a ser criada
     * @param  int $tempo tempo para sessão expirar
     * @return boolean       true|false
     */
    public static function criar(array $dados, $tempo = 60)
    {
        foreach($dados as $key => $dado)
        {
            Session::put($key, $dado);
        }

        if(is_array(Session::all()))
            return true;
        else
            return false;
    }

    /**
     * Renova os dados de uma sessão.
     * @param  array  $dados dados da sessão a ser renovada.
     * @param  int $tempo tempo para sessão expirar
     * @return boolean        true|false
     */
    public static function renovarSessao(array $dados, $tempo = 60)
    {
        return self::criar($dados, $tempo);
    }
}