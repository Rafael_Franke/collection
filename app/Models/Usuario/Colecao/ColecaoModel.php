<?php namespace App\Models\Usuario\Colecao;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;

class ColecaoModel extends Model
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'nome_colecao',
        'save_to'    => 'slug',
    ];

	public $timestamps = false;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'colecao';

	/**
	 * primary key of table.
	 * @var string
	 */
	protected $primaryKey = 'id_colecao';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id_usuario', 'nome_colecao', 'descricao', 'hashs', 'slug'];

}
