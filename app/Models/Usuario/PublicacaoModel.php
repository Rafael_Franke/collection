<?php namespace App\Models\Usuario;

use Illuminate\Database\Eloquent\Model;

class PublicacaoModel extends Model
{

	public $timestamps = false;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'publicacao';

	/**
	 * primary key of table.
	 * @var string
	 */
	protected $primaryKey = 'id_publicacao';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id_usuario', 'texto', 'localizacao', 'data_publicao'];

    /**
     * Usuário dessa publicação.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Usuario\UsuarioModel', 'id_usuario');
    }

    /**
     * Comentarios desta publicação.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comentarios()
    {
        return $this->hasMany('App\Models\Usuario\ComentarioModel', 'id_publicacao');
    }


    public function setTextoPublicacaoAttribute()
    {
    	return false;
    }

}
