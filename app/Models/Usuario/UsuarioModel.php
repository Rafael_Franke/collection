<?php namespace App\Models\Usuario;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class UsuarioModel extends Model implements AuthenticatableContract, CanResetPasswordContract
{

	use Authenticatable, CanResetPassword;


	public $timestamps = false;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuario';

	/**
	 * primary key of table.
	 * @var string
	 */
	protected $primaryKey = 'id_usuario';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['nome', 'sobrenome', 'email', 'password', 'site', 'data_cadastro', 'data_atualizacao'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	public function perfil()
	{
		return $this->hasOne('App\Models\Usuario\PerfilUsuarioModel', 'id_usuario');
	}

	/**
	 * Seguidores deste usuário.
	 */
	public function seguidores()
	{
		return $this->hasMany('App\Models\Usuario\UsuarioSeguirModel', 'id_usuario');
	}

}
