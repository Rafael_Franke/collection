<?php namespace App\Models\Usuario;

use Illuminate\Database\Eloquent\Model;

class ComentarioModel extends Model
{

	public $timestamps = false;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'comentario';

	/**
	 * primary key of table.
	 * @var string
	 */
	protected $primaryKey = 'id_comentario';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id_publicacao', 'id_usuario', 'comentario', 'data_hora'];

    /**
     * Usuário dessa publicação.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Usuario\UsuarioModel', 'id_usuario');
    }

}
