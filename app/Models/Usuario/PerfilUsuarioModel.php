<?php namespace App\Models\Usuario;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class PerfilUsuarioModel extends Model
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'perfil_usuario';

    /**
     * PrimaryKey
     * @var string
     */
    protected $primaryKey = 'id_perfil_usuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_usuario', 'sobre_voce', 'interesse', 'profissao', 'sou_fa'];

}
