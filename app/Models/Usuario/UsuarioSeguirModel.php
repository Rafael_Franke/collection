<?php namespace App\Models\Usuario;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class UsuarioSeguirModel extends Model
{

	public $timestamps = false;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuario_seguidor';

	/**
	 * primary key of table.
	 * @var string
	 */
	protected $primaryKey = 'id_usuario_seguidor';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['usuario_seguindo', 'id_usuario'];


}
