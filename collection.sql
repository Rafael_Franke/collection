-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 06/07/2015 às 03:20
-- Versão do servidor: 5.5.37-0ubuntu0.13.10.1
-- Versão do PHP: 5.5.3-1ubuntu2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `collection`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `colecao`
--

CREATE TABLE IF NOT EXISTS `colecao` (
  `id_colecao` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `nome_colecao` varchar(100) NOT NULL,
  `descricao` text,
  `hashs` text NOT NULL,
  `data_alteracao` datetime NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id_colecao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Fazendo dump de dados para tabela `colecao`
--

INSERT INTO `colecao` (`id_colecao`, `id_usuario`, `nome_colecao`, `descricao`, `hashs`, `data_alteracao`, `slug`) VALUES
(18, 75, 'Minha coleção de Filmes', 'Essa é minha coleção de filmes', '#colecao #filmes #bjc', '0000-00-00 00:00:00', ''),
(19, 75, 'Coleção de Action Figures', 'Essa é a coleção de action figures', '#colecao #actionfigures #bjc', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `id_comentario` int(11) NOT NULL AUTO_INCREMENT,
  `id_publicacao` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `comentario` text,
  `localizacao` varchar(255) NOT NULL,
  `data_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_comentario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Fazendo dump de dados para tabela `comentario`
--

INSERT INTO `comentario` (`id_comentario`, `id_publicacao`, `id_usuario`, `comentario`, `localizacao`, `data_hora`) VALUES
(11, 51, 75, 'ahahahahahah máximo!\n', '', '2015-07-04 20:10:01'),
(12, 51, 76, 'cara, que livro bom!\n', '', '2015-07-04 20:15:31'),
(13, 51, 75, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n', '', '2015-07-04 20:15:52'),
(14, 50, 75, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n', '', '2015-07-04 20:15:59'),
(15, 49, 75, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n', '', '2015-07-04 20:16:16'),
(16, 48, 75, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n', '', '2015-07-04 20:16:23'),
(17, 50, 75, 'ffasdf\n', '', '2015-07-04 20:22:43'),
(18, 51, 75, 'rafa\n', '', '2015-07-04 20:32:04'),
(19, 51, 75, 'fasdf\n', '', '2015-07-04 20:33:58'),
(20, 51, 75, 'fasdfasd\n', '', '2015-07-04 20:35:09'),
(21, 51, 75, 'fasdfasdf\n', '', '2015-07-04 20:36:06'),
(22, 51, 75, 'asdfasdf\n', '', '2015-07-04 20:36:32'),
(23, 51, 75, 'eita locooo\n', '', '2015-07-04 20:36:46'),
(24, 51, 75, 'fasfasdf\n', '', '2015-07-04 20:45:32'),
(25, 51, 75, '123456789\n', '', '2015-07-04 20:46:06'),
(26, 52, 75, 'eae galera!\n', '', '2015-07-05 03:22:03'),
(27, 52, 75, 'fasdfasdf\n', '', '2015-07-05 03:23:25'),
(28, 52, 75, 'fadf\n', '', '2015-07-05 03:23:58');

-- --------------------------------------------------------

--
-- Estrutura para tabela `config_usuario`
--

CREATE TABLE IF NOT EXISTS `config_usuario` (
  `id_config_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `twitter` varchar(150) DEFAULT NULL,
  `facebook` varchar(150) DEFAULT NULL,
  `youtube` varchar(150) DEFAULT NULL,
  `outras` text,
  `data_nascimento` date DEFAULT NULL,
  PRIMARY KEY (`id_config_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `desejo`
--

CREATE TABLE IF NOT EXISTS `desejo` (
  `id_desejo` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` text,
  `link` text,
  `loja_lugar` varchar(150) DEFAULT NULL,
  `imagem` varchar(150) DEFAULT NULL,
  `avisa_promocao` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id_desejo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `interesse`
--

CREATE TABLE IF NOT EXISTS `interesse` (
  `id_interesse` int(11) NOT NULL AUTO_INCREMENT,
  `id_perfil_usuario` int(11) NOT NULL,
  `interesse` varchar(255) NOT NULL,
  PRIMARY KEY (`id_interesse`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Fazendo dump de dados para tabela `interesse`
--

INSERT INTO `interesse` (`id_interesse`, `id_perfil_usuario`, `interesse`) VALUES
(29, 3, 'Coleções'),
(30, 3, ' action figures'),
(31, 3, ' selos'),
(32, 3, ' filmes'),
(33, 3, ' blu-rays'),
(34, 3, ' livros');

-- --------------------------------------------------------

--
-- Estrutura para tabela `item_colecao`
--

CREATE TABLE IF NOT EXISTS `item_colecao` (
  `id_item_colecao` int(11) NOT NULL AUTO_INCREMENT,
  `id_colecao` int(11) NOT NULL,
  `item_colecao` varchar(100) NOT NULL,
  `arquivo` varchar(100) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `numero_item` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_item_colecao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `perfil_usuario`
--

CREATE TABLE IF NOT EXISTS `perfil_usuario` (
  `id_perfil_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `sobre_voce` text,
  `interesse` varchar(150) DEFAULT NULL,
  `profissao` varchar(45) DEFAULT NULL,
  `sou_fa` text,
  PRIMARY KEY (`id_perfil_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Fazendo dump de dados para tabela `perfil_usuario`
--

INSERT INTO `perfil_usuario` (`id_perfil_usuario`, `id_usuario`, `sobre_voce`, `interesse`, `profissao`, `sou_fa`) VALUES
(3, 75, 'Perfil do Rafael Franke. Programando', 'action figures, cinema, filmes, blu-ray', 'Analista', 'Star Wars'),
(4, 76, 'Eae minha galera.', 'Redes sociais, pessoas, filmes, e etc', 'Empresa', 'Star Wars'),
(5, 75, 'Perfil do Rafael Franke. Programando', 'action figures, cinema, filmes, blu-ray', 'Analista', 'Star Wars'),
(6, 75, 'Perfil do Rafael Franke. Programando', 'action figures, cinema, filmes, blu-ray', 'Analista', 'Star Wars'),
(7, 77, '', '', '', ''),
(8, 78, '', '', '', ''),
(9, 79, 'acesse o site: blogdojotace.com.br', 'Coleções, action figures, selos, filmes, blu-rays, livros', '', 'Star Wars');

-- --------------------------------------------------------

--
-- Estrutura para tabela `publicacao`
--

CREATE TABLE IF NOT EXISTS `publicacao` (
  `id_publicacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `texto` text NOT NULL,
  `localizacao` varchar(255) NOT NULL,
  `data_publicacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nova` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_publicacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- Fazendo dump de dados para tabela `publicacao`
--

INSERT INTO `publicacao` (`id_publicacao`, `id_usuario`, `texto`, `localizacao`, `data_publicacao`, `nova`) VALUES
(33, 76, 'olá galera esse é meu perfil galera!', '', '2015-07-04 01:10:27', 0),
(34, 75, 'fasdfasdfasdf', '', '2015-07-04 00:27:07', 0),
(35, 79, 'olá galera aqui! ', '', '2015-07-04 00:27:07', 0),
(36, 75, 'rafael_frankean', '', '2015-07-04 00:27:07', 0),
(37, 75, 'de novo galera aqui é eu! ', '', '2015-07-04 00:27:07', 0),
(38, 79, 'eae galera aqui é teste!', '', '2015-07-04 00:27:07', 0),
(39, 75, 'kkkkkkkkkkkk', '', '2015-07-04 00:27:47', 0),
(40, 75, 'fasdfasfasdf', '', '2015-07-04 00:29:47', 0),
(41, 75, '54564654', '', '2015-07-04 00:33:58', 0),
(42, 75, 'fasdfasdfasdfasdf', '', '2015-07-04 01:10:27', 0),
(43, 75, 'fasdfasfasdf', '', '2015-07-04 01:14:17', 0),
(44, 75, 'asdfasdfasfasdf', '', '2015-07-04 01:14:38', 1),
(45, 75, 'olá galera! dinamico!', '', '2015-07-04 01:16:04', 1),
(46, 75, 'teste', '', '2015-07-04 01:26:12', 1),
(47, 79, 'teste', '', '2015-07-04 01:26:44', 1),
(48, 79, 'teste2', '', '2015-07-04 01:26:50', 1),
(49, 79, 'teste3', '', '2015-07-04 01:27:07', 1),
(50, 75, 'teste', '', '2015-07-04 03:14:56', 1),
(51, 76, 'isso ai galera', '', '2015-07-04 19:21:17', 1),
(52, 75, '\nfasdfasdf\n', '', '2015-07-04 20:46:45', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `nome` varchar(150) NOT NULL,
  `sobrenome` varchar(100) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `site` varchar(150) DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_atualizacao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

--
-- Fazendo dump de dados para tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `email`, `nome`, `sobrenome`, `login`, `password`, `site`, `data_cadastro`, `data_atualizacao`) VALUES
(75, 'rafaelfrankean@gmail.com', 'Rafael Franke', 'Franke', 'rafael_frankean', '$2y$10$n5WNtgDTxP6kz/S9JNjqs.E31dg8yhYxigYkx5Mj02B/Iz2lH6y0m', NULL, '2015-06-27 19:38:14', '2015-06-15 07:42:54'),
(76, 'conllectors@gmail.com', 'Conllectors', 'Connect', 'conllectors', '$2y$10$ctvRO.5PJ975EWm/hinstOol7V/UdlR2twANVV3NcJjV6j5URsPTq', NULL, '2015-06-30 03:31:39', '2015-06-17 07:01:37'),
(77, 'rafa.franke14@hotmail.com', 'Franke', 'Rafael', 'rafael', '$2y$10$sZYq1z5FX6MA9cKfkL5Mcuhd1iFbes9q/AI9wSTbd5p8p40DHJLie', NULL, '2015-06-30 03:31:45', '2015-06-20 23:53:56'),
(78, 'anounymousblood@gmail.com', 'Colecionador', 'Franke', 'franke', '$2y$10$0u959Rd8D4aYdCshKChBTOr6l4s70sVT1dfebLfOeyGzpWYZZ4j4u', NULL, '2015-06-30 03:31:48', '2015-06-20 23:58:57'),
(79, 'teste@teste.com.br', 'Teste', 'Test', 'teste', '$2y$10$kp4V4n32Oo6fu1R/GXxoC.k28KL19bOSyQst5tp65ePLniHqxo2aW', NULL, '2015-07-03 23:41:19', '2015-07-04 03:16:38');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuario_seguidor`
--

CREATE TABLE IF NOT EXISTS `usuario_seguidor` (
  `id_usuario_seguidor` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_seguindo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `data_acao` datetime NOT NULL,
  `data_atualizacao` datetime NOT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_usuario_seguidor`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=78 ;

--
-- Fazendo dump de dados para tabela `usuario_seguidor`
--

INSERT INTO `usuario_seguidor` (`id_usuario_seguidor`, `usuario_seguindo`, `id_usuario`, `data_acao`, `data_atualizacao`, `ativo`) VALUES
(77, 76, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(76, 78, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(75, 77, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(74, 79, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(73, 75, 76, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(72, 79, 76, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(71, 77, 79, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(70, 76, 79, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(69, 78, 79, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(68, 75, 79, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
