<!DOCTYPE html>
<html>
<head>
    <title>Collection of The World</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="css/padrao.css">
</head>
<body class="fotoFundo">
    <!--INICIO TOPO-->
    <div class="topo relative">
        <div class="container_12">
            <div class="logo grid_3">
                <h1>Collection of the World</h1>
            </div>
            <div class="areaBusca grid_5">
                <form class="formPadrao formBusca">
                    <fieldset>
                        <input type="text" class="inputPadrao borderRadius1" placeholder="Busque canais, perfis, coleções..." />
                    </fieldset>
                </form>
            </div>

            <div class="fotoPerfil borderRadius3 grid_4">
                <a href="#">
                    <img src="imagens/image2.jpg" height="50" width="50">
                </a>
            </div>

            <!--Barra de opções do usuário. Revisar -->
            <div class="opcoesUsuario grid_4 none">
                <ul>
                    <a href=""><li>Editar Perfil</li></a>
                    <a href=""><li>Coleção</li></a>
                </ul>
            </div>
        </div>
    </div>
    <!--FIM TOPO-->

    <div class="topoGerenciar conteudo">
        <div class="container_12">
            <div class="usuarioArea">
                <div class="fotoPerfil grid_4">
                    <a href="#">
                        <img src="imagens/image32.png" height="150" width="250" class="borderRadius3">
                    </a>
                </div>
                <div class="infoUsuario grid_5">
                    <big>Rafael Franke</big>
                    <p class="descricaoPerfil">Colecionador de filmes!</p>
                </div>
            </div>
        </div>
    </div>

    <?php
        include('include/menu.php');
    ?>

    <div class="gerenciarColecao conteudo container_12">
        <div>
            <div class="listagemColecao">
                <big class="font">Action Figures</big>
                <ul class="listagemPadrao">

                    <li class="adicionar">
                        <a href="#">
                            Adicionar
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>




</body>
</html>