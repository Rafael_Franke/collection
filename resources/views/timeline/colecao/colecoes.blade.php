@extends('layouts.default')

    @section('conteudo')


        @include('elements.capa_perfil')

        @include('elements.menu')

        <div class="gerenciarColecao conteudo container_12">
            <div>
                <div class="listagemColecao">
                    <big class="font">Coleções que você possue</big>
                    <ul class="listagemPadrao">

                        <li class="adicionar">
                            <a href="{{ route('adicionar_colecao') }}">
                                Adicionar
                            </a>
                        </li>
                        @foreach($colecoes as $colecao)
                            <li class="{{ !isset($colecao->imagem->capa) ? 'adicionar' : '' }}">
                                <a href="#">
                                    @if(isset($colecao->imagem->capa))
                                        <img src="imagens/image30.jpg" width="300" height="200">
                                    @else
                                        {{ $colecao->nome_colecao }}
                                    @endif

                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @stop