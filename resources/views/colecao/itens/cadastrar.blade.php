@extends('layouts.default')
    @section('conteudo')
        @include('elements.capa_perfil')
        @include('elements.menu')

        <div class="gerenciarColecao conteudo">
            <div class="container_12">
                <div class="listagemColecao">
                    <big class="font">Descreva seu colecionável</big>

                    <div class="formAdicionarColecaoItem">
                        <form action="#" class="formPadrao formAdicionarItem">
                            <div class="grid_12">
                                <input type="text" placeholder="Colecionável" class="input50" value="Os Vingadores" />
                                <input type="text" disabled placeholder="Ano de Lançamento" class="input50" value="2006" />
                                <select class="selectPadrao input40">
                                    <option></option>
                                    <option>Action figures</option>
                                    <option>Vingadores</option>
                                    <option selected>Filmes</option>
                                    <option>Carros</option>
                                    <option>Livros</option>
                                </select>
                                <textarea class="input100" placeholder="De detalhes sobre seu item.">Minha coleção de filmes.</textarea>

                                <textarea class="input100" placeholder="Curiosidades">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</textarea>
                                <div class="grid_4">
                                    <input type="checkbox" placeholder="Coleções relacionadas" class="input30" value="Action figures, livros, vingadores" />
                                    <label class="font">É uma edição númerada: </label>
                                </div>
                            </div>
                            <!-- <button class="btPadrao">Criar coleção</button> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @stop