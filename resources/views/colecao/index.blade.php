@extends('layouts.default')
    @section('conteudo')

        <div class="topoGerenciar conteudo">
            <div class="container_12">
                <div class="usuarioArea">
                    <div class="fotoPerfil grid_4">
                        <a href="#">
                            <img src="imagens/image32.png" height="150" width="250" class="borderRadius3">
                        </a>
                    </div>
                    <div class="infoUsuario grid_5">
                        <big>Rafael Franke</big>
                        <p class="descricaoPerfil">Colecionador de filmes!</p>
                    </div>
                </div>
            </div>
        </div>

    @include('elements.menu')

    <div class="gerenciarColecao conteudo">
        <div class="container_12">
            <div class="listagemColecao">
                <big class="font">Adicionados recentemente</big>
                <ul class="listagemPadrao">

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>  

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>  

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop