@extends('layouts.default')
    @section('conteudo')
        @include('elements.capa_perfil')
        @include('elements.menu')
        @include('elements.mensagem')
        <div class="gerenciarColecao conteudo">
            <div class="container_12">
                <div class="listagemColecao">
                    <big class="font tituloColecao">Nova Coleção</big>

                    <div class="formAdicionarColecaoItem">
                        <form action="{{ route('adicionar_colecao.post') }}" class="formPadrao formAdicionarItem formColecao">
                            <div class="grid_12">
                                <input type="text" placeholder="Coleção" name="nome_colecao" class="input50 colecao" />
                                <textarea class="input100" name="descricao" placeholder="Descreva sua coleção"></textarea>
                                <input type="text" name="hashs" placeholder="hashs: #colecao #minhacolecao" class="input30" />
                            </div>
                            <button class="btPadrao">Criar coleção</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @stop