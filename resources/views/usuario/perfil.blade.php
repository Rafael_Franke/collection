@extends('layouts.default')

    @section('conteudo')

        @include('elements.capa_perfil')

        @include('elements.menu')

        <div class="gerenciarColecao conteudo container_12">
            <div>
                <div class="listagemColecao">
                    <big class="font">Publicações de {{ $usuario->nome }}</big>
                    <ul class="listagemPadrao">
                        @foreach($publicacoes as $publicacao)
                            <li>
                                <a href="#">
                                    <img src="imagens/image1.jpg" width="300" height="200" >
                                </a>
                                <span class="textoPublicacao">{{ substr($publicacao->texto, 0, 100) }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @stop