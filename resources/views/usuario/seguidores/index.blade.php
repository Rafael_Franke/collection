@extends('layouts.default')
    @section('conteudo')
    @include('elements.menu')

        <div class="gerenciarColecao conteudo container_12">
           <div class="listagemColecao">
                <big class="font">Seguidores de {{ $usuario->nome }}</big>
                <ul class="listagemPadrao">
                    @if(count($seguidores) > 0)
                        @foreach($seguidores as $key => $seguidor)
                            <li>
                                <div class="fotoPerfil">
                                    <a href="{{ route('perfil.index', $seguidor->login) }}">
                                        <img src="{{ asset('imagens/image1.jpg') }}" width="100" height="100">
                                    </a>
                                </div>
                                <span class="nomeUsuario"><a href="#">{{ $seguidor->login }}</a></span>
                            </li>
                        @endforeach
                    @else
                        <big class="font">{{ $usuario->nome }} não possui seguidores.</big>
                    @endif
                </ul>
            </div>
        </div>
    @stop