@extends('layouts.layout_inicio')
@section('conteudo')
    <body class="login">
        <div class="pagLogin">
            @include('elements.colecoes')
            <div class="conteudo cadastro">
                @include('elements.topo1')
                <div class="form">
                    <form action="{{ route('cadastro.adicionar.post') }}" id="formCadastro" class="formPadrao formLogin formCadastro" method="POST">
                        <input type="text" name="nome" placeholder="Nome">
                        <input type="text" name="sobrenome" placeholder="Sobrenome">
                        <input type="text" class="email" name="email" placeholder="E-mail">
                        <input type="password" name="password" placeholder="Senha">
                        <span class="error_msg"></span>
                        <label class="labelPadrao">
                            <a class="font" target="_blank" href="{{ route('termo.index') }}">Aceito os termos de uso</a>
                        </label>
                        <input type="checkbox" class="checkboxPadrao" name="aceito">
                        <input type="hidden" class="_token" name="_token" value="{{ csrf_token() }}">
                        <button disabled class="btPadrao">Cadastrar</button>
                    </form>
                    @include('elements.direitos')
                </div>
            </div>
        </div>
        <!--FIM da TIMELINE-->
    </body>
@endsection