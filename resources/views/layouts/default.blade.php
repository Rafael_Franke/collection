<!DOCTYPE html>
<html>
<head>
    @include('elements.header')
</head>
<body class="fotoFundo">
    <!--INICIO TOPO-->
    <div class="topo relative">
        <div class="container_12">
            <div class="logo grid_3">
                <h1>Conllectors</h1>
            </div>
            <div class="areaBusca grid_5">
                <form class="formPadrao formBusca">
                    <fieldset>
                        <input type="text" class="inputPadrao borderRadius1" placeholder="Busque canais, perfis, coleções..." />
                    </fieldset>
                </form>
            </div>

            <div class="fotoPerfil borderRadius3 grid_4">
                <a href="#">
                    <img src="imagens/image2.jpg" height="50" width="50">
                </a>
            </div>

            <!--Barra de opções do usuário. Revisar -->
            <div class="opcoesUsuario grid_4 none">
                <ul>
                    <a href=""><li>Editar Perfil</li></a>
                    <a href=""><li>Coleção</li></a>
                </ul>
            </div>
        </div>
    </div>

    @yield('conteudo')

</body>
</html>
