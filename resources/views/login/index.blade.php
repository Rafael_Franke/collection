@extends('layouts.layout_inicio')
@section('conteudo')
    <body class="login">
        <div class="pagLogin">
           @include('elements.colecoes')
            <div class="conteudo">
               @include('elements.topo1')
                <div class="form">
                    <form action="{{ route('login.post') }}" class="formPadrao formLogin" method="POST">
                        <input type="text" name="login" placeholder="Usuário ou e-mail">
                        <input type="password" name="senha" placeholder="Senha">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                        <button class="btPadrao">Entrar</button>
                    </form>

                    <div class="cadastro">
                        <big><a class="font" href="{{ route('cadastro.index') }}">Cadastre-se</a></big>
                    </div>
                    @include('elements.direitos')
                </div>
            </div>
        </div>
    </body>
@endsection