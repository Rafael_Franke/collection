<div class="topoGerenciar conteudo">
    <div class="container_12">
        <div class="usuarioArea">
            <div class="fotoPerfil grid_4">
                <a href="#">
                    <img src="imagens/image32.png" height="150" width="250" class="borderRadius3">
                </a>
            </div>
            <div class="infoUsuario grid_5">
                <big>{{ isset($usuario) ? $usuario->nome : Session::get('nome') }}</big>

                <p class="descricaoPerfil">{{ isset($usuario) ? $usuario->perfil->sobre_voce : Session::get('descricao_perfil') }}</p>
            </div>
        </div>
    </div>
</div>