<title>Conllectors</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<base href="{{ url()  }}/public"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/padrao.css')}}">
<script type="text/javascript" src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/classes.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/validation/dist/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/jquery.autosize.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.bpopup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>