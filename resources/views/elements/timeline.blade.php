@foreach($publicacoes as $publicacao)
        <div class="conteudo container_12">
            <div class="timeLine grid_12">

                <div class="fotoPerfil grid_1">
                    <a href="#">
                        <img src="imagens/usuario_padrao.jpg" height="50" width="55">
                    </a>
                    <span class="nomeUsuario"><a href="{{ route('perfil_ver.index', $publicacao->usuario->login) }}">{{ $publicacao->usuario->login }}</a></span>
                </div>

                <div class="publicacao">
                    <div class="botoes none">
                        <a href="#" class="btUp">UP <span>(593)</span></a>
                    </div>
                    <a href="#"><img src="imagens/image1.jpg" width="650" height="500" class="itemColecao"></a>

                </div>
            </div>

            <div class="textoPublicacao">
                {{ $publicacao->texto }}
            </div>

            <div class="horario">
                <?php
                    if(date('d-m-Y', strtotime($publicacao->data_publicacao)) == date('d-m-Y'))
                    {
                        $data = 'Hoje às ' . date('H:i', strtotime($publicacao->data_publicacao));
                    }
                    else
                    {
                        $data = date('d/m H:i', strtotime($publicacao->data_publicacao));     
                    }
                ?>
                {{ $data }}
            </div>

            <div class="timeLine timeLineComentarios grid_12">

                <div class="comentarios">
                    <form class="formPadrao formComentario">
                        <textarea data-post="{{ $publicacao->id_publicacao }}" class="textAreaPadrao" placeholder="Deixe seu comentário"></textarea>
                    </form>
                </div>


                <div id="comentarios">
                    <div class="{{ $publicacao->id_publicacao }}"></div>
                    @foreach($publicacao->comentarios as $key => $comentario)
                        <div class="comentario">
                            <span class="nomeUsuario"><a href="{{ route('perfil_ver.index', $comentario->usuario->login) }}">{{ $comentario->usuario->login }}</a></span>
                            <div class="texto">
                                {{ $comentario->comentario }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            </div>
        </div>
    @endforeach