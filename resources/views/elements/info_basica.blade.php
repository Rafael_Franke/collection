<div class="infoUsuario conteudo container_12">
    <div class="info grid_8">
        <form class="formPadrao formInfoUsuario">
            <input type="text" name="Nome usuário" placeholder="Nome de Usuário">
            <input type="text" name="email" placeholder="Email">
            <input type="text" name="interesse" class="inputGrande" placeholder="Seus interesses: Action figures, Filmes, Carros...">
            <textarea name="descricao_perfil" class="textareaGrande" placeholder="Descrição do seu perfil"></textarea>
            <button class="btPadrao">Salvar</button>
        </form>
    </div>

    <div class="info grid_4">
        <div class="acoes">
            <a href="#">
                <span>Trocar foto de Perfil</span>
            </a>

            <a href="#">
                <span>Troca foto de capa</span>
            </a>

            <a href="#">
                <span>Foto de Fundo</span>
            </a>

            <a href="#">
                <span>Configurações de Perfil</span>
            </a>
        </div>
        <div class="fotoPerfil borderRadius3">
            <a href="#">
                <img src="imagens/image2.jpg" height="150" width="150">
            </a>
        </div>
    </div>
</div>