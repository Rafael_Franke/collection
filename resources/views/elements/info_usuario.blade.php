@include('elements.mensagem')
<div class="infoUsuario conteudo container_12">
    <div class="info grid_8">
        <form class="formPadrao formInfoUsuario" action="{{ route('perfil.alterar.post') }}" method="POST">
            <input type="text" name="nome_usuario" placeholder="Nome de Usuário" value="{{ $usuario->nome }}">
            <input type="text" name="email" placeholder="Email" value="{{ $usuario->email }}">
            <input type="text" name="login" class="login" data-url="{{ route('validar_login.post') }}" value="{{ $usuario->login }}" placeholder="Seu nome de usuário">
            <input type="text" name="interesse" class="inputGrande" value="{{ $usuario->perfil->interesse }}" placeholder="Seus interesses: Action figures, Filmes, Carros...">
            <input type="text" name="profissao" placeholder="Sua profissão(Não obrigatório)" value="{{ $usuario->perfil->profissao }}">
            <input type="text" name="sou_fa" placeholder="Sou fã de: " value="{{ $usuario->perfil->sou_fa }}">
            <textarea name="descricao_perfil" class="textareaGrande" placeholder="Descrição do seu perfil">{{ $usuario->perfil->sobre_voce }}</textarea>
            <button class="btPadrao">Salvar</button>
        </form>
    </div>

    <div class="modal grid_4">
        
    </div>

    <div class="info grid_4">
        <div class="acoes">
            <a href="#">
                <span>Trocar foto de Perfil</span>
            </a>

            <a href="#">
                <span>Troca foto de capa</span>
            </a>

            <a href="#">
                <span>Foto de Fundo</span>
            </a>

            <a href="#">
                <span>Configurações de Perfil</span>
            </a>
        </div>
        <div class="fotoPerfil borderRadius3 relative">
            <a href="{{ route('upload.index') }}" id="modal">
                <span class="btPadrao absolute block">Alterar Foto</span>
                <img src="imagens/image2.jpg" height="150" width="150">
            </a>
        </div>
    </div>
</div>