<!--Nova publicação-->
<div class="conteudo container_12">
    <div class="novaPublicacao grid_12">
        <form class="formPadrao formPublicacao" action="{{ route('publicar.post') }}" method="POST">
            <textarea class="textAreaPadrao" name="publicacao" class="publicacao" placeholder="Compartilhe um colecionável"></textarea>
            <div class="botoes">
                <span class="btPadrao"><a href="#">Adicionar Foto</a></span>
                <input type="submit" class="btPublicar" value="Publicar">
            </div>
        </form>
    </div>
</div>
<!--Fim nova publicação-->
