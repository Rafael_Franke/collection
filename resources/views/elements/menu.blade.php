<div class="menu conteudo">
    <div class="container_12">
        <ul class="listaMenu">
            <li class="{{ Route::current()->getName() == 'timeline.index' || Route::current()->getName() == 'perfil_ver.index' ? 'current' : ''  }}">
                <a href="{{ route('timeline.index') }}"><span>{{ isset($usuario) ? $usuario->nome : Session::get('login') }}</span></a>
            </li>
            <li class="{{ Route::current()->getName() == 'seguidores.index' ? 'current' : ''  }}">
                <a href="{{ route('seguidores.index', isset($usuario->login) ? $usuario->login : Session::get('login')) }}"><span>Seguidores</span></a>
            </li>
            <li>
                <a href="#"><span>Seguindo</span></a>
            </li>
            <li class="{{ Route::current()->getName() == 'explorar.index' ? 'current' : ''  }}">
                <a href="{{ route('explorar.index') }}"><span>Explorar</span></a>
            </li>
        
            <li>
                <a href="#"><span> {{ isset($usuario) && $usuario->id_usuario == Session::get('id_usuario') ? 'Mensagens' : 'Mensagem' }} </span></a>
            </li>
            

            <li>
                <a href="#"><span>Desejos</span></a>
            </li>
            <li class="{{ Route::current()->getName() == 'colecao.index' ? 'current' : ''  }}">
                <a href="{{ route('colecao.index') }}">
                    <span>
                        @if(!isset($usuario) || $usuario->id_usuario == Session::get('id_usuario'))
                            Minhas Coleções
                        @else
                            Coleções de {{ $usuario->nome }}
                        @endif
                    </span>
                </a>
            </li>
        </ul>
    </div>
</div>