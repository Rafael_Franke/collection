@extends('layouts.default')
    @section('conteudo')
    @include('elements.menu')

        <div class="gerenciarColecao conteudo container_12">
           <div class="listagemColecao">
                <big class="font">Colecinadores</big>
                <ul class="listagemPadrao">
                    @foreach($colecionadores as $key => $colecionador)

                        <li>
                            <div class="fotoPerfil">
                                <a href="{{ route('perfil.index', $colecionador->login) }}">
                                    <img src="{{ asset('imagens/image1.jpg') }}" width="100" height="100">
                                </a>
                            </div>
                            <span class="btSeguir borderRadius1">
                                <a data-seguindo="false" data-usuario="{{ $colecionador->login }}" href="javascript:void(0)">Seguir</a>
                            </span>
                            <span class="nomeUsuario"><a href="#">{{ $colecionador->login }}</a></span>
                        </li>

                    @endforeach
                </ul>
            </div>
        </div>
    @stop