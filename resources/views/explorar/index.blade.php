@extends('layouts.default')
    @section('conteudo')
    @include('elements.menu')

    <div class="gerenciarColecao conteudo container_12">
        <div>
            <div class="listagemColecao">
                <big class="font">Coleções</big>
                <ul class="listagemPadrao">

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Harry Potter</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Action Figures Marvel</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Hot Wheels</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Filmes</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Livros</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Carros</a></span>
                    </li>
                </ul>
            </div>

            @include('elements.vermais')

            <div class="listagemColecao">
                <big class="font">Assuntos</big>
                <ul class="listagemPadrao assuntos">

                    <li>
                        <a href="#" class="hashTag">
                            <span>#ActionFigures</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#BJC</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#Superman</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Batman</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Amazon</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#ActionFigures</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#BJC</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#Superman</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Batman</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Amazon</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Superman</span>
                        </a>
                    </li>

                        <li>
                        <a href="#" class="hashTag">
                            <span>#ActionFigures</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#BJC</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#Superman</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Batman</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Amazon</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#ActionFigures</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#BJC</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hashTag">
                            <span>#Superman</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Batman</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Amazon</span>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="hashTag">
                            <span>#Superman</span>
                        </a>
                    </li>

                </ul>
            </div>

            @include('elements.vermais')

            <div class="listagemColecao">
                <big class="font">Colecinadores</big>
                <ul class="listagemPadrao">

                    <li>
                        <a href="#">
                            <img src="imagens/image28.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Rafael Franke</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image1.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Exterminador</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image14.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Jeff Alves</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image28.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Rafael Franke</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image1.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Exterminador</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image14.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Jeff Alves</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image28.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Rafael Franke</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image1.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Exterminador</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image14.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Jeff Alves</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image28.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Rafael Franke</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image1.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Exterminador</a></span>
                    </li>

                    <li>
                        <a href="#">
                            <img src="imagens/image14.jpg" width="150" height="100">
                        </a>
                        <span class="nomeUsuario"><a href="#">Jeff Alves</a></span>
                    </li>
                </ul>
            </div>

            @include('elements.vermais')

            <div class="listagemColecao">
                <big class="font">Itens mais vistos</big>
                <ul class="listagemPadrao">

                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Harry Potter</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Action Figures Marvel</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Hot Wheels</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Filmes</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Livros</a></span>
                    </li>
                    <li>
                        <a href="#">
                            <img src="imagens/image30.jpg" width="300" height="200">
                        </a>
                        <span class="nomeUsuario"><a href="#">Carros</a></span>
                    </li>
                </ul>
            </div>
            @include('elements.vermais')
        </div>
    </div>

@stop