/**
 * Classe responsável por iteração relacionadas a coleção.
 * @constructor
 */
function Colecao()
{
    var acao = new Acao();
    var ajax = new Ajax();
    /**
     * Atualiza em tempo real o nome da coleção.
     */
    this.atualizarNomeColecao = function ()
    {
        $('.formColecao .colecao').keyup(function(){
            var valor = $(this).val() + ' ';
            acao.atualizarElemento('.tituloColecao', valor);
        });

        $('.formColecao .colecao').change(function(){
            var valor = $(this).val();
            acao.atualizarElemento('.tituloColecao', valor);
        });
    }


    this.cadastrarColecao = function()
    {
        $('.formColecao .btPadrao').click(function(e){
            e.preventDefault();
            acao.atualizarElemento('.formColecao .btPadrao', 'Aguarde..');

            var action = $('.formColecao').attr('action');

            var nome_colecao = $('input[name=nome_colecao]').val();
            var descricao = $('textarea[name=descricao]').val();
            var hashs = $('input[name=hashs]').val();

            var dados = {
                'nome_colecao': nome_colecao,
                'descricao': descricao,
                'hashs': hashs
            };

            ajax.post(action, dados, function(retorno){
                if(retorno.url)
                {
                    acao.atualizarElemento('.formColecao .btPadrao', 'Adicionar');
                    window.location.assign(retorno.url);
                }
                else
                {
                    var mensagem = new Mensagem();
                    mensagem.setMensagem(retorno);
                }

            });

        });
    }

}