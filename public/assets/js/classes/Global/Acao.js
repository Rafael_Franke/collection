/**
 * Classe responsável por executar ações relacionadas ao DOM.
 * @constructor
 */
function Acao()
{
    var timeout = new TimeOut();
    var ajax = new Ajax();

    /**
     * Atualiza o texto de um elemento.
     * @param elemento
     * @param valor
     */
    this.atualizarElemento = function(elemento, valor)
    {
        $(elemento).text(valor);
    }

    /**
     * Habilita ou desabilita um elemento.
     * @param elemento
     * @param boolean habilitar
     */
    this.habilitaDesabilitaElemento = function(elemento, habilitar)
    {
        $(elemento).prop('disabled', habilitar);
    }

    /**
     * Atualiza o valor de um input.
     * @param elemento
     * @param valor
     */
    this.atualizarElementoval = function(elemento, valor)
    {
        $(elemento).val(valor);
    }

    /**
     * Faz uma validação no texto digitado pelo usuário para validar se ele está marcando uma pessoa.
     * @param  jquery elemento elemento no qual será assistido.
     */
    this.verificTextoParaMarcarPessoa = function(elemento)
    {
        var i = 0;
        $(elemento).keypress(function(e){
            if(e.charCode == 64)
            {

            }
        });
    }

    this.rolagemTopo = function()
    {
        // FAZ A ROLAGEM DA BARRA DO TOPO EM PRECOS
        $(window).scroll(function () {
            var posy = $(document).scrollTop()
            if (posy > 350) {
                $(".topoFix").removeClass('none');
            } else {
                $(".topoFix").addClass('none');
            }
            $(".topoFix").animate({top: "0px"}, {
                duration: 500,
                queue: false

            });
        });
    }

    /**
     * Abre uma modal.
     * @return {[type]} [description]
     */
    this.modal = function()
    {
        var url = '';
        $('#modal').click(function(e){
            e.preventDefault();
            url = $(this).attr('href');
            $('.modal').bPopup({
                loadUrl: url 
            });
        });
    }

}