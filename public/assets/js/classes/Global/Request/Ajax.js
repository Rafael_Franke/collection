/**
 * Classe responsável por controlar o ajax.
 */
function Ajax()
{

    /**
     * Faz um request por POST
     * @param  {objeto} dados, dados para fazer o request.
     * @return {objeto}       retorna uma callback.
     */
    this.post = function(url, dados, callback) {
        $.ajax({
            url: url,
            method: 'POST',
            data: dados,
            success: function(resultado) {
                callback(resultado);
            }

        });
    }
}