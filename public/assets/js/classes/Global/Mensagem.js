/**
 * Classe responsável por controlar o as mensagem do usuário.
 */
function Mensagem()
{
    var time = new TimeOut();
    /**
     * Seta uma mensagem de aguarde para o usuário.
     * @param  jquery elemento   elemento a ser alterado.
     * @param  boolean text     ex: $('.elemento').text() será true se não false.
     * @param  String mensagem  Mensagem a ser colocada no elemento.
     */
    this.aguarde = function(elemento, text, mensagem) {
        mensagem = mensagem !== undefined ? mensagem : 'Aguarde...';
        if(text)
        {
            $(elemento).text(mensagem);
        }
        else
        {
            $(elemento).val(mensagem);
        }
    }

    /**
     * Seta alguma mensagem em um elemento.
     * @param  jquery elemento elemento a ser alterado.
     * @param  boolean text     ex: $('.elemento').text() será true se não false.
     * @param  String mensagem mensagem
     */
    this.setMensagem = function(retorno)
    {
        $('.mensagem').addClass(retorno.dados.classe);
        $('.mensagem big').text(retorno.dados.mensagem);
        $('.mensagem').toggle('drop');

        //time.setarTempo(function () {
        //    $('.mensagem').toggle('drop');
        //}, 5000);
    }
}