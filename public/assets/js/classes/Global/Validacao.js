/**
 * Classe responsável por controlar a validação de campos dinamicamente.
 * @constructor
 */
function Validacao()
{
    /**
     * Valida a existencia de um valor.
     * @param elemento
     * @param qndCarac
     * @param callback
     */
    this.validarValorExistenteKeyPress = function (elemento, qndCarac, url, callback) {
        $(elemento).keydown(function () {
            if($(this).val().length >= qndCarac)
            {
                var valorCampo = $(this).val();
                var valor = {'valor': valorCampo};
                var ajax = new Ajax();

                ajax.post(url, valor, function(retorno){
                    callback(retorno);
                });
            }
        });
    }
}
