/**
 * Classe responsável por tempos para executar alguma coisa.
 */
function TimeOut() {

    /**
     * Faz um request por POST
     * @param  {objeto} dados, dados para fazer o request.
     * @return {objeto}       retorna uma callback.
     */
    this.setarTempo = function(callback, tempo) {
        setTimeout(callback, tempo);
    }

    this.setarIntervalo = function(callback, tempo){
        setInterval(callback, tempo);
    }
}