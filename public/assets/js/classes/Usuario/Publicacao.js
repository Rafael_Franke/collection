/**
 * Classe responsável pelas ações e validações do usuário.
 */
function Publicacao()
{
    var ajax = new Ajax();
    var acao = new Acao();
    var time = new TimeOut();
    var usuario = $('input[name=login]').val();

    this.publicar = function()
    {
        $('.btPublicar').click(function(e){
            var bt = this;
            e.preventDefault();
            acao.habilitaDesabilitaElemento(this, true);
            acao.atualizarElementoval(this, 'Publicando...');

            var url = $('.formPublicacao').attr('action');

            var publicacao = $('textarea[name=publicacao]').val();
            var dados = {'publicacao': publicacao};
            ajax.post(url, dados, function(retorno){
                if(retorno.dados)
                {
                    $('textarea[name=publicacao]').val('');
                    acao.atualizarElemento('.novaPublicacao2 .nomeUsuario a', retorno.dados.usuario.login);
                    acao.atualizarElemento('.novaPublicacao2 .textoPublicacao', retorno.dados.publicacao.texto);
                    acao.atualizarElemento('.novaPublicacao2 .horario', retorno.dados.publicacao.data_publicacao);
                    $('.novaPublicacao2').addClass('bordaNovaPublicacao');
                    $('.novaPublicacao2').removeClass('none');
                }

            });

            time.setarTempo(function(){
                $('.novaPublicacao2').removeClass('bordaNovaPublicacao');
                acao.habilitaDesabilitaElemento(bt, false);
                acao.atualizarElementoval(bt, 'Publicar');
            }, 5000);

            
        });
    }

    /**
     * Comenta uma publicação.
     */
    this.fazerComentario = function()
    {
        $('.timeLineComentarios .comentarios .textAreaPadrao').keyup(function(e){
            if(e.which == 13)
            {
                var elemento = this;
                var comentario = $(this).val();
                var post = $(this).data('post');
                $(this).blur();
                $(this).prop('disabled', true);

                var html = "<div class='comentario'><span class='nomeUsuario'><a href='#'>"+ usuario +"</a></span><div class='texto'>"+comentario+"</div></div>";
                $('.' + post).append(html);

                var dados = {'comentario': comentario, 'post': post};

                ajax.post('usuario/comentar', dados, function(retorno){
                    $(elemento).val('');
                    $(elemento).prop('disabled', false);

                });

            }
        });
    }

    /**
     * Revisar
     */
    this.buscaAtualizacoesUsuarios = function () {
        var dados = {'atualizacoes': true};
        var classe = 'publicacao';
        time.setarIntervalo(function () {
            ajax.post('usuario/publicacoes', dados, function (retorno) {
                if (retorno.dados) {
                    $(retorno.dados).each(function (i, valor) {
                        classe = valor.usuario + ' none';
                        $('.novaPublicacao2').clone().addClass(classe)
                        acao.atualizarElemento(classe + ' .nomeUsuario a', valor.usuario);
                        acao.atualizarElemento(classe + ' .textoPublicacao', valor.texto);
                        $(classe).addClass('bordaNovaPublicacao');
                        $(classe).removeClass('none');
                    });
                }
            });
        }, 5000);
    }

}