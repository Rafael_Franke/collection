/**
 * Classe relacionada as ações do perfil do usuário.
 */
function Perfil()
{

    /**
     * Atualiza o perfil de um usuário.
     * @param  Obj   dados    dados que o usuário informou.
     * @param  {Function} callback dados adicionais.
     */
    this.atualizarPerfil = function(dados, callback)
    {
        var ajax = new Ajax();
        ajax.post('perfil/alterar-perfil', dados, function(retorno){
            $('.mensagem').addClass(retorno.dados.classe);
            $('.mensagem big').text(retorno.dados.mensagem);
            $('.mensagem').toggle('drop');
            callback();
        });
    }
}