/**
 * Classe responsável pelas ações e validações do usuário.
 */
function Usuario() {

    var aguardeMsg = 'Aguarde...';
    var ajax = new Ajax();
    var acao = new Acao();

    this.Usuario = function(aguardeMsg)
    {
        this.aguardeMsg = aguardeMsg;
    }

    /**
     * Valida formulário de Login.
     * @return true|false
     */
    this.validaFormLogin = function()
    {
        $('.formLogin').validate({
            rules: {
                login: {required: true},
                senha: {required: true}
            }
        });
    }

    /**
     * Valida formulário de cadastro.
     */
    this.validaFormCadastro = function()
    {
        $('.formCadastro').validate({
            rules: {
                nome: {required: true},
                sobrenome: {required: true},
                email: {required: true, email: true},
                password: {required: true}
            },

            messages: {
                nome: "",
                sobrenome: "",
                email: {required: ""},
                password: ""
            }
        });
    }

    /**
     * Valida email para cadastro. Verifica se email já está cadastrado.
     * @param  {string} elemento  elemento a ser validado
     * @param  {string} classePai classe pai
     * @return {boolean}           true|false
     */
    this.validaEmailCadastro = function(elemento, classePai)
    {
        var token = getToken(classePai);

        $(elemento).change(function(e){
            if($(this).val().length >= 6)
            {
                var email = $(this).val();
                var dados = {'email': email, '_token': token};

                var ajax = new Ajax();
                ajax.post('cadastrar/valida-email', dados, function(email){

                    if(email.existe)
                    {
                        $('.email').css({
                            'border': '1px solid red',
                            'background': '#F3A0A0'
                        });

                        $('.error_msg').text('E-mail já existente');
                        $('.error_msg').css('display', 'block');

                        habilidaDisabilitaCampo('.formPadrao .btPadrao', true);
                    }
                    else
                    {
                        $('.email').css({
                            'border': '1px solid #408B31',
                            'background': '#C3DAB9'
                        });
                        $('.error_msg').css('display', 'none');
                        habilidaDisabilitaCampo('.formPadrao .btPadrao', false);
                    }
                });
            }
        });
    }

    /**
     * Atualizar perfil usuário.
     */
    this.atualizarPerfilUsuario = function()
    {
        $('.formInfoUsuario .btPadrao').click(function(e){
            var bt = this;
            $(this).prop('disabled', true);
            var textoElem = $(this).text();
            var msg = new Mensagem();
            msg.aguarde(bt, true);

            e.preventDefault();
            var nome_usuario, email, login, interesse, profissao, sou_fa, descricao_perfil, dados;
            nome_usuario = $('.formInfoUsuario input[name=nome_usuario]').val();
                email = $('.formInfoUsuario input[name=email]').val();
            login = $('.formInfoUsuario input[name=login]').val();
            interesse = $('.formInfoUsuario input[name=interesse]').val();
            profissao = $('.formInfoUsuario input[name=profissao]').val();
            sou_fa = $('.formInfoUsuario input[name=sou_fa]').val();
            descricao_perfil = $('.formInfoUsuario textarea[name=descricao_perfil]').val();

            dados = {
                'nome_usuario': nome_usuario,
                'email': email,
                'login': login,
                'interesse': interesse,
                'profissao': profissao,
                'sou_fa': sou_fa,
                'sobre_voce': descricao_perfil
            };

            var perfil = new Perfil();
            perfil.atualizarPerfil(dados, function(){
                msg.setMensagem(bt, true, textoElem);
                var time = new TimeOut();
                time.setarTempo(function(){
                    $('.mensagem').toggle('drop');
                    $(bt).prop('disabled', false);
                }, 5000);
            });
        });
    }

    /**
     * Valida a existencia de um login no banco de dados.
     */
    this.validarNomeLogin = function()
    {
        var validar = new Validacao();
        var url = $('.formInfoUsuario .login').data('url');

        validar.validarValorExistenteKeyPress('.formInfoUsuario .login', 5, url, function(retorno){
            if(!retorno.existeLogin)
            {
                $('.formInfoUsuario .login').removeClass('error');
                $('.formInfoUsuario .btPadrao').prop('disabled', false);
            }
            else
            {
                $('.formInfoUsuario .btPadrao').prop('disabled', true);
                $('.formInfoUsuario .login').addClass('error');
            }
        });
    }

    /**
     * Segue um novo usuário.
     */
    this.seguirUsuario = function()
    {
        $('.btSeguir a').click(function(e){
            e.preventDefault();
            var bt = this;
            var usuario = $(this).data('usuario');

            var dados = {
              'usuario': usuario
            };
            
            if($(this).attr('data-seguindo') == 'false')
            {
                acao.atualizarElemento(bt, 'Aguarde...');
                ajax.post('usuario/seguir', dados, function(retorno){
                    if(!retorno.seguiu)
                    {
                        alert('não foi possível seguir.');
                    }
                    else 
                    {
                        acao.atualizarElemento(bt, 'Seguindo');
                        $(bt).attr('data-seguindo', 'true');
                    }
                });
            }
        });
    }


}