
/**
 * Habilita ou disabilita um campo.
 */
function habilidaDisabilitaCampo(elemento, valor)
{
    $(elemento).prop('disabled', valor);
}

/**
 * Pega a token para enviar via ajax.
 * @param  {[type]} classe [description]
 * @return {[type]}        [description]
 */
function getToken(classe)
{
    return $(classe).find('input[name=_token]').val();
}

/**
 * Redimenciona qualquer campo.
 * @param  {class} campo campo a ser redimencionado automaticamente.
 */
function redimencionalCampo(campo)
{
    $(campo).autosize();
}

$(document).ready(function() {
    // CSRF protection
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });

    /**
     * Redimenciona campo de nova publicação
     */
    redimencionalCampo('.formPadrao textarea');
    
    redimencionalCampo('.formPadrao.formComentario .textAreaPadrao');

    /**
     * Valida algum campo em tempo de excução.
     */
    // validaEmailCadastro('.formPadrao input.email', '.formPadrao');
});
