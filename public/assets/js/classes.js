$(document).ready(function() {

    /**
     * Importa biblioteca do ajax.
     */
    $.getScript("assets/js/classes/Global/Request/Ajax.js", function(){
        var ajax = new Ajax();
    });

    /**
     * Importa biblioteca do ajax.
     */
    $.getScript("assets/js/classes/Global/Validacao.js", function(){
        var validar = new Validacao();
    });

    /**
     * Importa classe de timeout
     */
    $.getScript('assets/js/classes/Global/TimeOut.js', function () {
        var time = new TimeOut();
    });

    /**
     * Importa biblioteca do ajax.
     */
    $.getScript("assets/js/classes/Global/Acao.js", function(){
        var acao = new Acao();
        acao.rolagemTopo();
        acao.modal();
    });


    /**
     * Importa classe do usuário.
     */
    $.getScript('assets/js/classes/Usuario/Usuario.js', function(){
        var usuario = new Usuario();
        usuario.validaFormCadastro();
        usuario.validaFormLogin();
        usuario.validaEmailCadastro('.formPadrao input.email', '.formPadrao');
        usuario.atualizarPerfilUsuario();
        usuario.validarNomeLogin();
        usuario.seguirUsuario();
    });

    /**
     * Importa classe de publicação.
     */
    $.getScript('assets/js/classes/Usuario/Publicacao.js', function(){
        var publicacao = new Publicacao();
        publicacao.publicar();
        publicacao.fazerComentario();
    });

    /**
     * Importa classe do Perfil.
     */
    $.getScript('assets/js/classes/Usuario/Perfil.js', function(){
        var perfil = new Perfil();
    });

    /**
     * Importa classe do Perfil.
     */
    $.getScript('assets/js/classes/Colecao/Colecao.js', function(){
        var colecao = new Colecao();
        colecao.atualizarNomeColecao();
        colecao.cadastrarColecao();
    });



    /**
     * Importa classe de timeout
     */
    $.getScript('assets/js/classes/Global/Mensagem.js', function(){
        var mensagem = new Mensagem();
    });


});