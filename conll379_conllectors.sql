-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 02/07/2015 às 15:09:24
-- Versão do Servidor: 5.5.40-36.1
-- Versão do PHP: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `conll379_conllectors`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `colecao`
--

CREATE TABLE IF NOT EXISTS `colecao` (
  `id_colecao` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `nome_colecao` varchar(100) NOT NULL,
  `descricao` text,
  `hashs` text NOT NULL,
  `data_alteracao` datetime NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id_colecao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Extraindo dados da tabela `colecao`
--

INSERT INTO `colecao` (`id_colecao`, `id_usuario`, `nome_colecao`, `descricao`, `hashs`, `data_alteracao`, `slug`) VALUES
(18, 75, 'Minha coleção de Filmes', 'Essa é minha coleção de filmes', '#colecao #filmes #bjc', '0000-00-00 00:00:00', ''),
(19, 75, 'Coleção de Action Figures', 'Essa é a coleção de action figures', '#colecao #actionfigures #bjc', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
  `id_comentario` int(11) NOT NULL AUTO_INCREMENT,
  `id_publicacao` int(11) NOT NULL,
  `comentario` text NOT NULL,
  `localizacao` varchar(255) NOT NULL,
  `data_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_comentario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `config_usuario`
--

CREATE TABLE IF NOT EXISTS `config_usuario` (
  `id_config_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `twitter` varchar(150) DEFAULT NULL,
  `facebook` varchar(150) DEFAULT NULL,
  `youtube` varchar(150) DEFAULT NULL,
  `outras` text,
  `data_nascimento` date DEFAULT NULL,
  PRIMARY KEY (`id_config_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `desejo`
--

CREATE TABLE IF NOT EXISTS `desejo` (
  `id_desejo` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` text,
  `link` text,
  `loja_lugar` varchar(150) DEFAULT NULL,
  `imagem` varchar(150) DEFAULT NULL,
  `avisa_promocao` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id_desejo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `interesse`
--

CREATE TABLE IF NOT EXISTS `interesse` (
  `id_interesse` int(11) NOT NULL AUTO_INCREMENT,
  `id_perfil_usuario` int(11) NOT NULL,
  `interesse` varchar(255) NOT NULL,
  PRIMARY KEY (`id_interesse`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Extraindo dados da tabela `interesse`
--

INSERT INTO `interesse` (`id_interesse`, `id_perfil_usuario`, `interesse`) VALUES
(25, 3, 'action figures'),
(26, 3, ' cinema'),
(27, 3, ' filmes'),
(28, 3, ' blu-ray');

-- --------------------------------------------------------

--
-- Estrutura da tabela `item_colecao`
--

CREATE TABLE IF NOT EXISTS `item_colecao` (
  `id_item_colecao` int(11) NOT NULL AUTO_INCREMENT,
  `id_colecao` int(11) NOT NULL,
  `item_colecao` varchar(100) NOT NULL,
  `arquivo` varchar(100) NOT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `numero_item` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_item_colecao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil_usuario`
--

CREATE TABLE IF NOT EXISTS `perfil_usuario` (
  `id_perfil_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `sobre_voce` text,
  `interesse` varchar(150) DEFAULT NULL,
  `profissao` varchar(45) DEFAULT NULL,
  `sou_fa` text,
  PRIMARY KEY (`id_perfil_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `perfil_usuario`
--

INSERT INTO `perfil_usuario` (`id_perfil_usuario`, `id_usuario`, `sobre_voce`, `interesse`, `profissao`, `sou_fa`) VALUES
(3, 75, 'Perfil do Rafael Franke. Programando', 'action figures, cinema, filmes, blu-ray', 'Analista', 'Star Wars'),
(4, 76, 'Eae minha galera.', 'Redes sociais, pessoas, filmes, e etc', 'Empresa', 'Star Wars'),
(5, 75, 'Perfil do Rafael Franke. Programando', 'action figures, cinema, filmes, blu-ray', 'Analista', 'Star Wars'),
(6, 75, 'Perfil do Rafael Franke. Programando', 'action figures, cinema, filmes, blu-ray', 'Analista', 'Star Wars'),
(7, 77, '', '', '', ''),
(8, 78, '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacao`
--

CREATE TABLE IF NOT EXISTS `publicacao` (
  `id_publicacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `texto` text NOT NULL,
  `localizacao` varchar(255) NOT NULL,
  `data_publicacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_publicacao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Extraindo dados da tabela `publicacao`
--

INSERT INTO `publicacao` (`id_publicacao`, `id_usuario`, `texto`, `localizacao`, `data_publicacao`) VALUES
(1, 75, '', '', '2015-06-30 02:40:22'),
(2, 75, '', '', '2015-06-30 03:15:21'),
(3, 75, '', '', '2015-06-30 03:15:57'),
(4, 75, 'Olá galera essa é a minha publicação.', '', '2015-06-30 03:36:21'),
(5, 76, 'fadfkjasdkfsakdfjasdfasdfsadf', '', '2015-07-01 20:05:26'),
(6, 76, 'Rafael Franke', '', '2015-07-01 20:06:53'),
(7, 76, 'Rafael Franke', '', '2015-07-01 20:07:19'),
(8, 76, 'Rafael Franke', '', '2015-07-01 20:07:40'),
(9, 76, 'teste', '', '2015-07-01 20:09:47'),
(10, 76, 'fasdfasdf', '', '2015-07-01 20:10:21'),
(11, 76, 'fasdfasdf', '', '2015-07-01 20:11:32'),
(12, 76, 'fasdfsafsad', '', '2015-07-01 20:11:41'),
(13, 76, 'fasdfasdfasdfsadf', '', '2015-07-01 20:12:29'),
(14, 76, 'Olá galera essa são minha publicações', '', '2015-07-01 20:17:15'),
(15, 76, 'teste', '', '2015-07-01 20:17:57'),
(16, 76, 'teste para display block', '', '2015-07-01 20:21:53'),
(17, 76, 'olá galera daqui do mundo.', '', '2015-07-01 20:29:06'),
(18, 76, 'Rafael Franke aqui do mundo.', '', '2015-07-01 20:29:51'),
(19, 76, 'Rafael Fraqnke teste aqui do conllectors', '', '2015-07-01 20:30:30'),
(20, 76, 'Teste RAfael Franke daqui', '', '2015-07-01 20:31:00'),
(21, 76, 'fasdfasdfasdfasdfasdfasdf', '', '2015-07-01 20:35:16'),
(22, 76, 'fasdfsadfsafdasf', '', '2015-07-01 20:35:56'),
(23, 76, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', '2015-07-01 20:36:50'),
(24, 76, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', '2015-07-01 20:46:41'),
(25, 76, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', '2015-07-01 20:47:33'),
(26, 76, 'teste aqui c', '', '2015-07-01 21:02:28'),
(27, 76, 'FASDJKFHSAHFJSADKJFASDF', '', '2015-07-01 21:07:04'),
(28, 76, 'Olá, galera! Salve! Estou ingressando aqui no Conllectors e queria saber qual é melhor forma compartilhar minha coleção com vocês. Abraços.', '', '2015-07-01 21:11:34'),
(29, 75, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', '2015-07-02 13:12:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `nome` varchar(150) NOT NULL,
  `sobrenome` varchar(100) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `site` varchar(150) DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data_atualizacao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=79 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `email`, `nome`, `sobrenome`, `login`, `password`, `site`, `data_cadastro`, `data_atualizacao`) VALUES
(75, 'rafaelfrankean@gmail.com', 'Rafael Franke', 'Franke', 'rafael_frankean', '$2y$10$n5WNtgDTxP6kz/S9JNjqs.E31dg8yhYxigYkx5Mj02B/Iz2lH6y0m', NULL, '2015-06-27 19:38:14', '2015-06-15 07:42:54'),
(76, 'conllectors@gmail.com', 'Conllectors', 'Connect', 'conllectors', '$2y$10$ctvRO.5PJ975EWm/hinstOol7V/UdlR2twANVV3NcJjV6j5URsPTq', NULL, '2015-06-30 03:31:39', '2015-06-17 07:01:37'),
(77, 'rafa.franke14@hotmail.com', 'Franke', 'Rafael', 'rafael', '$2y$10$sZYq1z5FX6MA9cKfkL5Mcuhd1iFbes9q/AI9wSTbd5p8p40DHJLie', NULL, '2015-06-30 03:31:45', '2015-06-20 23:53:56'),
(78, 'anounymousblood@gmail.com', 'Colecionador', 'Franke', 'franke', '$2y$10$0u959Rd8D4aYdCshKChBTOr6l4s70sVT1dfebLfOeyGzpWYZZ4j4u', NULL, '2015-06-30 03:31:48', '2015-06-20 23:58:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_seguidor`
--

CREATE TABLE IF NOT EXISTS `usuario_seguidor` (
  `id_usuario_seguidor` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_seguindo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `data_acao` datetime NOT NULL,
  `data_atualizacao` datetime NOT NULL,
  `ativo` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_usuario_seguidor`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Extraindo dados da tabela `usuario_seguidor`
--

INSERT INTO `usuario_seguidor` (`id_usuario_seguidor`, `usuario_seguindo`, `id_usuario`, `data_acao`, `data_atualizacao`, `ativo`) VALUES
(38, 76, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(37, 75, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(36, 78, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(35, 78, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(34, 77, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(33, 77, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(32, 77, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(31, 77, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(30, 76, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(29, 76, 75, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
